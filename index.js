require("dotenv").config();
const mongoose = require('mongoose');
const data = require("./data");
const { CronJob } = require('cron')
const morgan = require('morgan');
const PFMExternalNotifier = require("./pfmexternalnotifier")
const ElnexuNotifier = require("./elnexunotifier")
// const TlvTags =  require('./TLVTags.json');

// const JournalServices = require("./db/sql/services/journalServices");
// const terminalService = require("./db/sql/services/terminalServices");
// const bankconfigServices = require("./db/sql/services/bankconfigServices")
// const bandA = require("./config/bandA.json");

// const notification = require("./config/notification.json");

// //const virtualTids = require("./config/virtual_tids.json");


const nodemailer = require("nodemailer");
const json2xls = require('json2xls');
const moment = require('moment-timezone');
const fs = require('fs');
const path = require('path');
const Journals = require('./model/journamodel');
const registeredNotificationModel = require("./model/registerednotificationmodel");
// const BankConfig = require('./model/bankconfigmodel');
// const PfmNotifier = require('./transactionNotifier');
const ETZNotifier = require("./etznotifier");
// const FlutterNotifier = require("./flutternotifier");
// const ThreeLineNotifier = require('./threelinesNotifier');
// const WactNotifier = require('./wactnotifier');
// const JasipayNotifier = require('./jasipaynotifier');
// const HSMNoitifer = require("./hsmNotifier");
// const CellulantNotifier = require("./cellulantNotifier");
// const PayFrontierNotifier = require("./payfrontiernotifier");

// const MikroNotifier = require("./mikronotifier");

// const ExchangeboxNotifier = require("./exchangeboxnotifier");
// const IgrParkwayNotifier = require("./igrparkway");
// const journalModel = require("./model/journamodel");
// const registeredNotificationModel = require("./model/registerednotificationmodel");
// const Util = require("./helpers/Utils");
// const bankConfig = require("./helpers/bank-config");

// const EboxNotifier = require("./eboxnotifer");
// const TransactionNotifier = require("./transactionNotifier");




const reversalResponse = {
    batchNo: "4960092227860522",
    tranNo: "496009222",
    resCode: "00",
    status: "4300148041",
    message: "510101511344101",
    rrn: "510101511344101"
}


const unpackedMessage = { mti: "0200", dataElements: {"2":"4960092227860522","3":"011000","4":"000000020000","5":null,"6":null,"7":"0921114110","8":null,"9":null,"10":null,"11":"114110","12":"114110","13":"0921","14":"2010","15":null,"16":null,"17":null,"18":"6010","19":null,"20":null,
"21":null,"22":"051","23":"000","24":null,"25":"00","26":"04","27":null,"28":"D00000000","29":null,"30":null,"31":null,"32":"496009","33":null,"34":null,"35":"4960092227860522D20102261982237200000",
"36":null,"37":"200921114043","38":null,"39":null,"40":"226","41":"2UP1T007","42":"2UP1LA000007ITX","43":"ITEX INTERGRATED SERVICLA           LANG","44":null,"45":null,"46":null,"47":null,"48":null,"49":"566","50":null,"51":null,
"52":null,"53":null,"54":null,
"55":"9F26089EF05863F939DA3F9F2701809F100706010A03A4A8029F3704EAB349179F360202DC950500800080009A032009219C01009F02060000000000005F2A02056682023C009F3303E0E8F05F3401009F3501229F34034103029F1A020566","56":null,"57":null,"58":null,
"59":"hsm~1000138081","60":"010085C24300148041Meter Number=12.87001001.Acct=0001451023.Phone=07038085747","61":null,"62":null,"63":null,"64":null,"65":null,"66":null,"67":null,"68":null,"69":null,"70":null,
"71":null,"72":null,"73":null,"74":null,"75":null,"76":null,"77":null,"78":null,"79":null,"80":null,"81":null,"82":null,"83":null,"84":null,"85":null,"86":null,"87":null,"88":null,"89":null,"90":null,"91":null,"92":null,"93":null,"94":null,"95":null,"96":null,
"97":null,"98":null,"99":null,"100":null,"101":null,"102":null,"103":null,"104":null,"105":null,"106":null,"107":null,"108":null,"109":null,"110":null,"111":null,"112":null,"113":null,"114":null,"115":null,"116":null,"117":null,"118":null,"119":null,"120":null,"121":null,"122":null,
"123":"510101511344101","124":null,"125":null,"126":null,"127":null,"128":"D71798F8EB12ACAF3746AAE498089E7F7A1178B4493ECECB7CB2B4A0E3457AE3"} };


const unpackedHandlingServerMessage = { mti: "0210", dataElements: {"2":"4960092227860522","3":"011000","4":"000000020000","5":null,"6":null,"7":"0921114110","8":null,"9":null,"10":null,"11":"114110","12":"114110","13":"0921","14":"2010","15":null,"16":null,"17":null,"18":"6010","19":null,"20":null,
"21":null,"22":"051","23":"000","24":null,"25":"00","26":"04","27":null,"28":"D00000000","29":null,"30":null,"31":null,"32":"496009","33":null,"34":null,"35":"4960092227860522D20102261982237200000",
"36":null,"37":"200921114043","38":"87001001","39":"00","40":"226","41":"2UP1T007","42":"2UP1LA000007ITX","43":"ITEX INTERGRATED SERVICLA           LANG","44":null,"45":null,"46":null,"47":null,"48":null,"49":"566","50":null,"51":null,
"52":null,"53":null,"54":null,
"55":"9F26089EF05863F939DA3F9F2701809F100706010A03A4A8029F3704EAB349179F360202DC950500800080009A032009219C01009F02060000000000005F2A02056682023C009F3303E0E8F05F3401009F3501229F34034103029F1A020566","56":null,"57":null,"58":null,
"59":"hsm~1000138081","60":"010085C24300148041Meter Number=12.87001001.Acct=0001451023.Phone=07038085747","61":null,"62":null,"63":null,"64":null,"65":null,"66":null,"67":null,"68":null,"69":null,"70":null,
"71":null,"72":null,"73":null,"74":null,"75":null,"76":null,"77":null,"78":null,"79":null,"80":null,"81":null,"82":null,"83":null,"84":null,"85":null,"86":null,"87":null,"88":null,"89":null,"90":null,"91":null,"92":null,"93":null,"94":null,"95":null,"96":null,
"97":null,"98":null,"99":null,"100":null,"101":null,"102":null,"103":null,"104":null,"105":null,"106":null,"107":null,"108":null,"109":null,"110":null,"111":null,"112":null,"113":null,"114":null,"115":null,"116":null,"117":null,"118":null,"119":null,"120":null,"121":null,"122":null,
"123":"510101511344101","124":null,"125":null,"126":null,"127":null,"128":"D71798F8EB12ACAF3746AAE498089E7F7A1178B4493ECECB7CB2B4A0E3457AE3"} };





const notifservice2 ={
    "name": "Cellulant Notification",
    "url": "http://41.73.252.230:28080/SmartWallet/Proxy/notification",
    "notificationClass": "cellulantnotifier",
    "enabled": true,
    "authorizationToken": "CELL!:c3llul4nt!!@#",
    "reversalUrl": "http://41.73.252.230:28080/SmartWallet/Proxy/reversal"
}

const notifservice3 ={
        "name": "Jasipay notification",
        "url": "https://api.funditnigeria.com/api/v1/transaction/cashout",
        "notificationClass": "jaispaynotifier",
        "key": "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9",
        "enabled": true,
        "liveUrl": "https://api.funditnigeria.com/api/v1/transaction/cashout"
    }

    // const notifservice ={
    //     "name": "EBox notification",
    //     "url": "https://myebucksapi.herokuapp.com/api/pos-ebucks-webhook/",
    //     "notificationClass": "eboxnotifer",
    //     "authorizationToken": "Basic aXRleC1wb3M6SXRleFBPUyQxMjM=",
    //     "enabled": true,
    //     "liveUrl": "https://myebucksapi.herokuapp.com/api/pos-ebucks-webhook/"
    // }

    // const notifservice = {
    //     "name": "PFM Notification",
    //     "url": "https://www.pfm.payvice.com/api/tms/iisys/auth2",
    //     "reversalUrl": "https://www.pfm.payvice.com/api/tms/iisys/auth2",
    //     "notificationClass": "default",
    //     "enabled": true
    // }

    const notifservice = {
        "name": "Payfrontier Notification",
        "url": "https://api.getfyba.com/rbq/v1/pos/itex/terminal_notification",
        "reversalUrl": "https://api.getfyba.com/rbq/v1/pos/itex/terminal_notification",
        "notificationClass": "payfrontiernotifier",
        "authorizationToken": "'Bearer payfrontier_itex_live_d7h33dd34h4k4343638403050efsf434hh4k5287853",
        enabled: true
    }

const testtransaction = {
    "vasData": null,
    "receipt": "paper",
    "receiptSent": false,
    "rrn": "000603105235",
    "onlinePin": true,
    "merchantName": "ONYESCOM VENTURES LTD ",
    "merchantAddress": "KD           LANG",
    "merchantId": "FBP205600444741",
    "terminalId": "2058LS73",
    "STAN": "105235",
    "transactionTime": "2020-10-17T09:51:34.400Z",
    "merchantCategoryCode": "5621",
    "handlerName": "NIBSS POSVAS",
    "MTI": "0200",
    "maskedPan": "533477XXXXXX4549",
    "processingCode": "000000",
    "amount": 100,
    "currencyCode": "566",
    "messageReason": "Approved",
    "originalDataElements": null,
    "customerRef": "hsm~Test Cardholder",
    "TVR": "0000240800",
    "CRIM": "660C333A8C9FF016",
    "__v": 0,
    "FIIC": "557694",
    "authCode": "779130",
    "failOverRrn": "",
    "handlerResponseTime": "2020-10-17T09:51:34.400Z",
    "handlerUsed": "POSVAS",
    "interSwitchResponse": "",
    "oldResCode": "",
    "responseCode": "00",
    "script": "9F360200AB910a2476EB460B68373C0012",
    "tamsBatchNo": "",
    "tamsMessage": "",
    "tamsRRN": "",
    "tamsStatus": "",
    "tamsTransNo": "",
    "write2pos": "00"
}

async function flutterEmailReport(file) {
    //let file = "Report/transaction-flutter-report-Tue Oct 13 2020.xlsx";
    if (file == false)
        return false;

    let transporter = nodemailer.createTransport({
        
        host: process.env.SMTP_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.SMTP_USER, // generated ethereal user
            pass: process.env.SMTP_PASS // generated ethereal password
        }
    });

    let date = new Date().toDateString();

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"ITEX EFT-ENGINE" <i-alert@iisysgroup.com>', // sender address
        to: process.env.flutter_emails, // list of receivers
        bcc : "taiwo.oladapo@iisysgroup.com,bolaji.oyerinde@iisysgroup.com", 
        subject: "ITEX-FLUTTER Transaction Report for " + date, // Subject line
        text: "Hi, Download ITEX-FLUTTER transaction report for " + date,
        attachments: [{
            path: file
        }]
    });

    
    return info;

}

async function prepareReportFromIMS() {

    let option = {
        fields: [
            "handlerUsed", "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","notified", "pfmNotifed"
        ]
    };

    const reports = require("./config/report.json");

    const newReports = [];

    let count = 0

    for (let report of reports) {

        var time = report.transactionTime;
        let t = time.split(" ")[0];

        let re = t.split("/");

        let newDateHandler = `${re[re.length - 1]}-${re[re.length - 3]}-${re[re.length - 2]} ${time.split(" ")[1]}`

        var testDateUtc = moment(newDateHandler);
        var localDate = moment(testDateUtc).local();

        var s = localDate.format("YYYY-MM-DD HH:mm:ss");
        var d = localDate.toDate().toString();


        time = report.handlerResponseTime;
        t = time.split(" ")[0];

        re = t.split("/");

        newDateHandler = `${re[re.length - 1]}-${re[re.length - 3]}-${re[re.length - 2]} ${time.split(" ")[1]}`

        testDateUtc = moment(newDateHandler);
        localDate = moment(testDateUtc).local();

        s = localDate.format("YYYY-MM-DD HH:mm:ss");
        d2 = localDate.toDate().toString();


        let newObject = { ...report }

        newObject.transactionTime = d;
        newObject.handlerResponseTime = d2;

        newReports.push(newObject);

        count++

        console.log(d, count)

    }


    let xls = json2xls(newReports,option);

    let date = new Date().toDateString();

    let filesPath = `Report/transaction-report-${date}.xlsx`;
    let pathDir = path.dirname(filesPath);
    if (fs.existsSync(pathDir) == false) {
        fs.mkdirSync(pathDir)
    }
    fs.writeFileSync(filesPath,xls, 'binary');

    console.log(filesPath)
    return filesPath;
}


async function checkerNotifier(terminalId, notificationService) {

    const notificationTerminalIds = notification.map(i => i["All My Terminals"]);

    return await registeredNotificationModel.find({terminalId, notificationService});

}


async function addTerminalForNotification(notificationService) {

    const records = [];

    for (const i of notificationTerminalIds) {

        console.log(i)

        const checkifExists = await checkerNotifier(i, notificationService);

        console.log({terminalId: i, notificationService})
        // console.log(checkifExists)

        // if(checkifExists === null) {

        //     records.push({
        //         "name": "PFM Notification",
        //         "enabled": true,
        //         notificationService,
        //         "terminalId": i
        //     });

        // }

    }


    console.log(records.length);

    // registeredNotificationModel.insertMany(records, (err, doc) => {

    //     if(err) return false;

    //     console.log("Records successfully inserted!")
    //     console.log(doc)

    // }) 




}


async function connectDatabase() {

    const {
        databaseDriver,
        databaseHost,
        databasePort,
        databaseUser,
        databasePwd,
        databaseCollection,
        databaseHost_2,
        databasePort_2,
        databaseHost_3,
        databasePort_3,
        replicaSet
    } = {

        databaseDriver: process.env.DATABASE_DRIVER || 'mongodb',
        databaseHost: process.env.DATABASE_HOST || 'localhost',
        databasePort: process.env.DATABASE_PORT || '27017',
        databaseUser: process.env.DATABASE_USER || 'eft-user',
        databasePwd: process.env.DATABASE_PWD || '4839!!Itex',
        databaseCollection: process.env.DATABASE_COLLECTION || 'eftEngine',

        databaseHost_2: process.env.DATABASE_HOST_2 || 'localhost',
        databasePort_2: process.env.DATABASE_PORT_2 || '27017',

        databaseHost_3: process.env.DATABASE_HOST_3 || 'localhost',
        databasePort_3: process.env.DATABASE_PORT_3 || '27017',
        replicaSet: process.env.REPLICA_SET || 'rs0'

    }

    if (process.env.APP_ENV == "local" && process.env.APP_DEBUG) {

        console.log(`Database Connection: ${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

    }

    console.log(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`)

    mongoose.Promise = global.Promise;
    //mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort},${databaseHost_2}:${databasePort_2},${databaseHost_3}:${databasePort_3}/${databaseCollection}`, {
    mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`, {
    // mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}?authSource=admin`, {
        useNewUrlParser: true,
        // sets how many times to try reconnecting
        reconnectTries: Number.MAX_VALUE,
        // sets the delay between every retry (milliseconds)
        reconnectInterval: 1000,
        autoReconnect : true,
        numberOfRetries : 5,
        keepAlive: 300000, connectTimeoutMS: 300000,
        // replset name
        replicaSet,
        eadPreference: 'secondaryPreferred',
        poolSize:process.env.POOL_SIZE || 8
         
    },()=>{
        // rs.secondaryOk()
        console.log("Connected now")
        // this.setUpEmailCron();

        // this.setUpDataSocket();
    });

}

async function updateBankConfigToBandA() {

    const terminalIds = bandA.map(i => i["Terminal ID"]);

    // console.log(terminalIds)

    // { $set: { group: "A"} },


    await BankConfig.updateMany({terminalId : {$in: terminalIds }}, { $set: { group: "A"} }, (err, doc) => {


            if(err) {

                console.log("An error has occured" + err.toString())

            }

            console.log(doc);

        }
        );



}


async function generateExchangeBoxReport() {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode"
        ]
    };

    try {
        let startDate = moment('20200903', 'YYYYMMDD').startOf('day').toDate();
        let endDate = moment('20200904', 'YYYYMMDD').endOf('day').toDate();

        // // for live

        // let startDate = moment().startOf('day').toDate();
        // let endDate = moment().endOf('day').toDate();

        let report = await Journals.find(
            { terminalId : {$in: ["2033GDV1"]} , responseCode : "00", transactionTime: {
                $gte: startDate,
                $lt: endDate
            }}, option.fields).sort('-terminalId');

        console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        if (report.length <= 0)
            return false;
        
        let xls = json2xls(report,option);

        let date = new Date().toDateString();

        let filesPath = `Report/transaction-exchange-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath,xls, 'binary');
        return filesPath;
    } catch (error) {
        console.error(error);
        return false;
    }
}


async function generateC24Report() {

        let option = {
            fields: [
                "rrn", "merchantName", "merchantAddress", "merchantId",
                "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
                "MTI", "maskedPan", "processingCode", "amount", "messageReason",
                "responseCode", "authCode","notified"
            ]
        };

        try {
            let startDate = moment('20211102', 'YYYYMMDD').startOf('day').toDate();
            // console.log("start", startDate);
            let endDate = moment('20211102', 'YYYYMMDD').endOf('day').toDate();

            // // for live
            // let startDate = moment().startOf('day').toDate();
            // let endDate = moment().endOf('day').toDate();


            // console.log("running query")
            // let terminalId = "2033JLI4"

            let report = await Journals.find({
                $or: [{
                    customerRef: {
                        $regex: "^c24"
                    }
                }, {
                    customerRef: {
                        $regex: "^C24"
                    } 
                }],
                responseCode: "00",
                // terminalId,
                transactionTime: {
                    $gte: startDate,
                    $lt: endDate
                }
            }, option.fields).sort('-terminalId');


            // console.log(
            //     {
            //         customerRef: {
            //             '$regex': "^C24"
            //         } ,
            //          responseCode: '00',
            //     transactionTime: { '$gte': new Date("2021-08-16"), '$lt': new Date("2021-08-16") }
            //   });


            console.log("hereh")

            console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

            if (report.length <= 0)
                return false;
            
            let xls = json2xls(report,option);

            let date = new Date().toDateString();

            let filesPath = `Report/C24-transaction-report-11th-Nov-2021.xlsx`;
            // let filesPath = `Report/C24-transaction-report-${date}.xlsx`;
            let pathDir = path.dirname(filesPath);
            if (fs.existsSync(pathDir) == false) {
                fs.mkdirSync(pathDir)
            }
            fs.writeFileSync(filesPath,xls, 'binary');
            return filesPath;
        } catch (error) {
            console.error(error);
            return false;
        }
}



async function C24EmailReport() {
    try {
const intervalSec = '* * * * * *' // cron job runs every seconds
const interval = '30 2 * * *' // cron job runs everyday at 2:30am
let job = new CronJob(
  interval,
  async function () {
    try {
     let file = await generateC24Report();
     console.warn(`C24 report ${JSON.stringify(file)}`);
    //  console.log("file", file);
     if (file == false)
         return false;
         let transporter = nodemailer.createTransport({
            host: process.env.SMTP_HOST,
             port: 465,
             secure: true, // true for 465, false for other ports
             auth: {
                 user: process.env.SMTP_USER, // generated ethereal user
                 pass: process.env.SMTP_PASS // generated ethereal password
             },
             tls: {
                 rejectUnauthorized: false
             }
         });
         transporter.verify((err, success) => {
             if (err) console.error(err);
             else {
 
                 console.log('Your config is correct');
             }
         });
         console.log("verify", transporter.options.host);
 
         let date = new Date().toDateString();
     
         // send mail with defined transport object
         let info = await transporter.sendMail({
             from: '"ITEX EFT-ENGINE" <i-alert@iisysgroup.com>', // sender address
             to: process.env.c24_emails, // list of receivers
            //  bcc : "alayesanmifemi@gmail.com", 
             subject: `ITEX-C24 Transaction Report for ${date}`, // Subject line
             text: `Hi, Download ITEX-C24 transaction report for ${date}`,
             attachments: [{
                 path: file
             }]
         });
         return info;
    } catch (error) {
      console.log(error)
    }
  },
  null,
  true,
  "Africa/Lagos"
);
job.start()
morgan.token('date', () => new Date().toLocaleString());
process.env.TZ = 'Africa/Lagos';
         
    
        
    } catch (error) {
        console.log("error", error);
    }
}

async function generatePayantReport() {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","pfmNotified"
        ]
    };

    try {
        let startDate = moment('20200701', 'YYYYMMDD').startOf('day').toDate();
        // let endDate = moment('20200701', 'YYYYMMDD').endOf('day').toDate();

        // // for live

        // let startDate = moment().startOf('day').toDate();
        let endDate = moment().endOf('day').toDate();

        let report = await Journals.find(
        {terminalId : {$in: ["2058VX02","2058VX03","2058VX04","2058VX05","2058VX06","2058VX07","2058VX08","2058VX09","2058VX10","2058VX11","2058VX12","2058VX13","2058VX14","2058VX15","2058VX16","2058VX17","2058VX18","2058VX19","2058VX20","2058VX21"]},
        responseCode : "00",transactionTime: {
            $gte: startDate,
            $lt: endDate
        }}, option.fields).sort('-terminalId');

        console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        if (report.length <= 0)
            return false;

        let xls = json2xls(report, option);

        let date = new Date().toDateString();

        let filesPath = `Report/payant-transaction-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath, xls, 'binary');
        return filesPath;
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function getTerminalReportsByMonth() {

    let startDate = moment('20201017', 'YYYYMMDD').startOf('day').toDate();

    let endDate = moment('20201117', 'YYYYMMDD').startOf('day').toDate();

    console.log(startDate)

    console.log(endDate)

    let report = await Journals.aggregate([
        {$match: {
            transactionTime: {
                $gte: startDate,
                $lt: endDate
            }
        }},
        { $group: { _id: "$terminalId", totalAmount: { $sum: "$amount" }, totalCount: { $sum: 1 } } },
        //{$count: "totalCount"},
        { $project: {
            // terminalId: "$_id",
            totalAmount: "$totalAmount",
            totalCount: "$totalCount"
        }},
        { $sort: { "_id": -1 } }
    ]);

    console.log("report");

    if (report.length <= 0)
    return false;

    let option = ["_id", "totalAmount", "totalCount"]

    let xls = json2xls(report, option);

    let date = new Date().toDateString();

    let filesPath = `Report/terminal-transaction-report-${date}.xlsx`;
    let pathDir = path.dirname(filesPath);
    if (fs.existsSync(pathDir) == false) {
        fs.mkdirSync(pathDir)
    }
    fs.writeFileSync(filesPath, xls, 'binary');
    return filesPath;


}

async function sendtestnotification() {



    const eboxnotifer = new PayFrontierNotifier(notifservice, testtransaction);

   const response =  await eboxnotifer.sendNotification();

   return response
}

async function generateAccessFailoverFailedReport() {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","tamsRRN","tamsStatus","tamsMessage"
        ]
    };

    try {
        let startDate = moment('20201006', 'YYYYMMDD').startOf('day').toDate();
        let endDate = moment('20201006', 'YYYYMMDD').endOf('day').toDate();

        // // for live

        // let startDate = moment().startOf('day').toDate();
        // let endDate = moment().endOf('day').toDate();
        let handler  = "MW-FEP";

        let report = await Journals.find(
        {$or:[{terminalId : {$regex : /^2063/ }},{terminalId : {$regex : /^2044/ }}],
        responseCode : {$ne : "00"}, 
        //tamsStatus: {$ne : "331"}, 
        handlerUsed: handler,
        transactionTime: {
            $gte: startDate,
            $lt: endDate
        }
    }, option.fields).sort('-terminalId');

        console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        if (report.length <= 0)
            return false;

        // let mappedData = this.mapAccessbankdataToReport(report);

        console.log(report)

        //process.exit();

        let xls = json2xls(report, option);
        // let xls = json2xls(mappedData);

        let date = new Date().toDateString();

        let filesPath = `Report/failover-failed-transaction-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath, xls, 'binary');
        return filesPath;
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function sendManualPFMNotificationXavier(data) {

    // const notificationService = {
    //     "name": "Exchangebox",
    //     "url": "http://197.253.19.75:8029/vas/push-pos-notifications",
    //     "notificationClass": "exchangeboxnotifier",
    //     "enabled": true
    // }

    // {terminalId: "2232PT45", maskedPan: "506105XXXXXXXXX2301"}

    // const notificationService = {
    //     "name": "Flutterwave Notification",
    //     "url": "https://flutterwaveprodv2.com/flwvpos/api/pos/itex/notification",
    //     "notificationClass": "flutternotifier",
    //     "enabled": true,
    //     "authorizationToken": "bGtfdWd3WWFlQlJsaTo="
    // }

    const notificationService = {
        "name": "ETZ CASHOUT",
        "url": "https://www.etranzact.net/cashpoint/api/cashpoint/v1/transactions",
        "notificationClass": "etznotifier",
        "enabled": true,
        "key": "ITEX",
        "authorizationToken": "aXRleDpJOHR4OSMyMXlo"
    }

    // const notificationService = {
    //     "name": "MIKRO notification",
    //     "url": "https://itex.mikro.africa/terminal/mk-itex-notify",
    //     "reversalUrl": "https://itex.mikro.africa/terminal/mk-itex-reversal",
    //     "notificationClass": "mikronotifier",
    //     "authorizationToken": "aXRleDpORFUyTUdGb2NYVjNaamx6ZG1FSw==",
    //     "enabled": true
    // }




    let startDate = moment('20201118', 'YYYYMMDD').startOf('day').toDate();

    let endDate = moment('20201118', 'YYYYMMDD').endOf('day').toDate();

    console.log("Here");
    
    //for(let transaction of data) {

        Journals.find({terminalId: "22057H68N", rrn: "210412092937", maskedPan: "539983XXXXXX2676",
        transactionTime: {
            $gte: startDate,
            $lt: endDate
        }
        }, (err, doc) => {


            // Journals.find({rrn: "000033591420", terminalId: "20325U1X", MTI: "0200", STAN: "080309"}  , (err, doc) => {

            console.log(doc)

            for(data of doc) {

            

                if(doc !== null) {

                    const exchangeboxnotifier = new ETZNotifier(notificationService, data);


                    exchangeboxnotifier.sendNotification();

                }
                    
    
            }

            //}

         


        })


    //}


}

function getICCData(nibssICC) {
    // let nibssICC = unpackedMessage.dataElements[55];

    if (nibssICC) {
        let iccDataList = new Map();
        let skip = 0;
        while (skip < nibssICC.length) {
            let tag = {};
            tag = TlvTags.find(c => c.tag == nibssICC.substr(skip, 2));
            if (tag) {
                skip = skip + 2;
            } else {
                tag = TlvTags.find(c => c.tag == nibssICC.substr(skip, 4));
                skip = skip + 4;
            }

            let length = nibssICC.substr(skip, 2);
            length = (Number.parseInt(length, 16)) * 2;
            skip = skip + 2;
            let data = nibssICC.substr(skip, length);
            // console.log(`tag: ${tag.tag}, data: ${data}`);
            skip = skip + length;
            iccDataList.set(tag.tag, data);
        }

        return iccDataList;
    }
    return false;
}

async function generateFlutterReport() {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","notified"
        ]
    };

    try {
        let startDate = moment('20211008', 'YYYYMMDD').startOf('day').toDate();
        let endDate = moment('20211008', 'YYYYMMDD').endOf('day').toDate();

        // let startDate = moment('20201129', 'YYYYMMDD').startOf('day').toDate();

        // const startDate = moment().format('YYYY-MM-DD');
        // const endDate= moment().format('YYYY-MM-DD');
      
        console.log(startDate)

        // // for live

        // let startDate = moment().startOf('day').toDate();

        // let endDate = moment().endOf('day').toDate();

        // console.log(endDate)

        console.log("Here now!")
        let report = await Journals.find(
        {
            responseCode : "00",
            customerRef : {$regex : /^flutter/ },transactionTime: {
            $gte: startDate,
            $lt: endDate
        }}, option.fields).sort('-terminalId');

        // console.log("report", report);

        
        // let terminalId = "2215166A"
        // let rrn = "201827111947"
        // const report = await Journals.find(
        //     {
        //         // terminalId, 
        //         // rrn,
        //     transactionTime: {
        //         $gte: startDate,
        //         $lt: endDate
        //     }
        // }, option.fields).sort('-terminalId');

        console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        if (report.length <= 0)
            return false;

        let xls = json2xls(report, option);

        let date = new Date().toDateString();

        let filesPath = `Report/flutter-transaction-report.xlsx`;
        // let filesPath = `Report/flutter-transaction-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath, xls, 'binary');
        return filesPath;
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function processMiddleWareTAMSTransaction(Terminal){
    let clearPinkey = ExtractKeys.getDecryptedPinKey(Terminal.pinKey_1, Terminal.masterKey_1, 1);

    let hashKey = ExtractKeys.getDecryptedSessionKey(Terminal.sessionKey_1,Terminal.masterKey_1, 1);
    
    let requestData = {};
    Object.assign(requestData, this.unpackedMessage.dataElements);
    let customerRef = requestData[59] || "";
    requestData[59] = `${this.config.name}${clearPinkey}~` + customerRef;


    process.exit();

    let isoRequestData = ExtractKeys.rehashUnpackedIsoMessage(requestData,this.isoPacker,hashKey);

    let clientSocket = new SocketClient(this.Tams_IP,this.Tams_Port,true);

    let socketHandler = clientSocket.startClient(isoRequestData, 180000);

    let self = this;
    return new Promise(
        function (resolve, reject) {

            socketHandler.on('data', data => {
                console.log(data.toString())
                console.log(`middleware TAMS responded RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
                Util.fileDataLogger(Util.getTerminalForLog(self.unpackedMessage),`middleware TAMS responded RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
                
                socketHandler.end();

                resolve(data);
            });

            socketHandler.on('error', err => {
                console.log(`middleware TAMS ${err.message}`);
                Util.fileDataLogger(Util.getTerminalForLog(self.unpackedMessage),`middleware TAMS ${err.message}`);

                reject(err);
            });
            socketHandler.on('timeout', () => {
                console.error(`middleware TAMS TIMEDOUT RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
                Util.fileDataLogger(Util.getTerminalForLog(self.unpackedMessage),`middleware TAMS TIMEDOUT RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
                
                reject(`middleware TAMS TIMEDOUT RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
            });
            socketHandler.on('close', () => {
                console.error(`middleware TAMS close connection, RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
                Util.fileDataLogger(Util.getTerminalForLog(self.unpackedMessage), `middleware TAMS close connection, RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);

                reject(`middleware TAMS close connection, RRN ${Util.getRRN(self.unpackedMessage)} terminal ${Terminal.terminalId} at ${new Date().toString()}`);
            });
        }

    );

}

//MIKRO Report
async function generateMikroReport() {

    let option = {
    fields: [
    "rrn", "merchantName", "merchantAddress", "merchantId",
    "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
    "MTI", "maskedPan", "processingCode", "amount", "messageReason",
    "responseCode", "authCode","notified"
    ]
    };
    
    try {
    let startDate = moment('20210412', 'YYYYMMDD').startOf('day').toDate();
    let endDate = moment('20210412', 'YYYYMMDD').endOf('day').toDate();
    
    // // for live
    
    // let startDate = moment().startOf('day').toDate();
    // let endDate = moment().endOf('day').toDate();
    
    console.log("here now");
    
    const report = await Journals.find({
        $or: [{
            customerRef: {
              $regex: "^mikr"
            }
            }, {
            customerRef: {
              $regex: "^mikro"
            }
        }],
        transactionTime: {
              $gte: startDate,
              $lt: endDate
            }
      });
    // let report = await Journals.find({
    // $or: [{
    // customerRef: {
    // $regex: "^mikr"
    // }
    // }, {
    // customerRef: {
    // $regex: "^mikro"
    // }
    // }],
    // transactionTime: {
    // $gte: startDate,
    // $lt: endDate
    // }
    // }, option.fields).sort('-terminalId');
    
    console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);
    
    // report.forEach(rep => {
    // rep['amount'] = rep.amount / 100;
    // });
    
    if (report.length <= 0)
    return false;
    
    let xls = json2xls(report,option);
    
    let date = new Date().toDateString();
    
    let filesPath = `Report/mikro-transaction-report-04-12-April-2021.xlsx`;
    let pathDir = path.dirname(filesPath);
    if (fs.existsSync(pathDir) == false) {
    fs.mkdirSync(pathDir)
    }
    fs.writeFileSync(filesPath,xls, 'binary');
    return filesPath;
    } catch (error) {
    console.error(error);
    return false;
    }
    }


async function generateEtranzactReport() {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","notified"
        ]
    };

    try {
        // let startDate = moment('20210417', 'YYYYMMDD').startOf('day').toDate();
        // let endDate = moment('20210417', 'YYYYMMDD').endOf('day').toDate();

        let startDate = moment('20210420', 'YYYYMMDD').startOf('day').toDate();
        console.log("start", startDate);
        let endDate = moment('20210420', 'YYYYMMDD').endOf('day').toDate();
        console.log("end", endDate);
        // // for live

        // let startDate = moment().startOf('day').toDate();
        // let endDate = moment().endOf('day').toDate();

        // console.log(startDate)
        let report = await Journals.find({
            $or: [{
                customerRef: {
                    $regex: "^etz"
                }
            }, {
                customerRef: {
                    $regex: "^ETZ"
                }
            }, {terminalId: {$in: 
                ['2070QJ44', '2070QJ89', '2070QJ63', '2070QJ66', '2070QK19', '2070QM35', '2070QJ92']}}],
            transactionTime: {
                $gte: startDate,
                $lt: endDate
            }
        }, option.fields).sort('-terminalId');

        // console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        option.fields.push('notificationResponse');

        report.forEach(rep => {
            rep['amount'] = rep.amount / 100;
            rep['notificationResponse'] = rep.notified;

        });


        if (report.length <= 0)
            return false;
        
        let xls = json2xls(report,option);

        let date = new Date().toDateString();

        let filesPath = `Report/etz-transaction-report-20.xlsx`;
        // let filesPath = `Report/etz-transaction-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath,xls, 'binary');
        return filesPath;
    } catch (error) {
        console.error(error);
        return false;
    }
}

async function EtranzactEmailReport() {
    let file = await generateEtranzactReport();
    if (file == false)
        return false;

    let transporter = nodemailer.createTransport({
        host: process.env.SMTP_HOST,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: process.env.SMTP_USER, // generated ethereal user
            pass: process.env.SMTP_PASS // generated ethereal password
        }
    });

    let date = new Date().toDateString();

    // send mail with defined transport object
    let info = await transporter.sendMail({
        from: '"ITEX EFT-ENGINE" <i-alert@iisysgroup.com>', // sender address
        to: process.env.etz_emails, // list of receivers
        bcc : "femi.alayesanmi@iisysgroup.com", 
        subject: "ITEX-ETRANZACT Transaction Report for " + "Wed Nov 11 2020 GMT+0100 (West Africa Standard Time)", // Subject line
        text: "Hi, Download ITEX-ETRANZACT transaction report for " + "Wed Nov 11 2020 GMT+0100 (West Africa Standard Time)",
        attachments: [{
            path: file
        }]
    });
    return info;
}

async function sendManualPFMNotification(data) {

    // const notificationService = {
    // "name": "Exchangebox",
    // "url": "http://197.253.19.75:8029/vas/push-pos-notifications",
    // "notificationClass": "exchangeboxnotifier",
    // "enabled": true
    // }
    
    // {terminalId: "2232PT45", maskedPan: "506105XXXXXXXXX2301"}
    
    // const notificationService = {
    // "name": "Flutterwave Notification",
    // "url": "https://flutterwaveprodv2.com/flwvpos/api/pos/itex/notification",
    // "notificationClass": "flutternotifier",
    // "enabled": true,
    // "authorizationToken": "bGtfdWd3WWFlQlJsaTo="
    // }
    
    // const notificationService = {
    // "name": "ETZ CASHOUT",
    // "url": "https://www.etranzact.net/cashpoint/api/cashpoint/v1/transactions",
    // "notificationClass": "etznotifier",
    // "enabled": true,
    // "key": "ITEX",
    // "authorizationToken": "aXRleDpJOHR4OSMyMXlo"
    // }
    
    // const notificationService = {
    // "name": "MIKRO notification",
    // "url": "https://itex.mikro.africa/terminal/mk-itex-notify",
    // "reversalUrl": "https://itex.mikro.africa/terminal/mk-itex-reversal",
    // "notificationClass": "mikronotifier",
    // "authorizationToken": "aXRleDpORFUyTUdGb2NYVjNaamx6ZG1FSw==",
    // "enabled": true
    // }
    
    // const notificationService = {
    // "name": "FRSC Notification Sterling",
    // "url": "https://pg.innovate1pay.com/app/PosService?wsdl",
    // "notificationClass": "manual-frsc-sterling",
    // "enabled": true
    // }
    
    const notificationService = {
    "name": "Kudi Withdrawal",
    "url": "https://pollux.kudi.ai/pos/callbacks/itex",
    "key": "",
    "reversalUrl": "https://pollux.kudi.ai/pos/callbacks/itex/reversals",
    "notificationClass": "pfmexternalnotifier",
    "authorizationToken": "eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1MjM2Mzc0NDUsInN1YiI6ImFkbWluQGt1ZGkuYWkiLCJyb2xlcyI6ImFkbWluIiwiZXhwIjoxNTIzNjU5MDQ1fQ.kpgUgrzc00JyV-1nDKnXgzxX_LQdmTdMuGKyHpUidy8",
    "enabled": true
    }
    
    
    let startDate = moment('20210625', 'YYYYMMDD').startOf('day').toDate();
    
    let endDate = moment('20210625', 'YYYYMMDD').endOf('day').toDate();
    
    // console.log("Here");
    
    //for(let transaction of data) {
    
    Journals.find({ 
        rrn:"210625141625", 
        terminalId:"2057H21O",
    transactionTime: {
    $gte: startDate,
    $lt: endDate
    }
    }, async (err, doc) => {
    
    
    // Journals.find({rrn: "000033591420", terminalId: "20325U1X", MTI: "0200", STAN: "080309"} , (err, doc) => {
    
    console.log("doc", doc)
    
    for(data of doc) {
    
    
    
    if(doc !== null) {
    
    
    const exchangeboxnotifier = new PFMExternalNotifier(notificationService,data);
    
    
    exchangeboxnotifier.sendNotification();
    
    }
    
    
    }
    
    //}
    
    
    
    
    })
    
    
    //}
    
    
    }


async function sendManualElnexuNotifier(data) {    
    const notificationService = {
    "name":"Elnexu Notification",
    "url": "https://backoffice.elnexuconsult.com/api/webhook/itex/transaction",
    "reversalUrl": "",
    "authorization": "2|v2GCTUlmsKT5oDoLDqTNJE9sXGRAjFiBwtE2gi5q",
    "enabled": true,
    "notificationClass": "elnexunotifier"
    }
        
    let startDate = moment('20211004', 'YYYYMMDD').startOf('day').toDate();
    
    let endDate = moment('20211004', 'YYYYMMDD').endOf('day').toDate();
    
    console.log("Here");
    
    //for(let transaction of data) {
    
    Journals.find({ 
        // rrn:"210921112834", 
        terminalId:"20320CH0",
    // transactionTime: {
    // $gte: startDate,
    // $lt: endDate
    // }
    }, async (err, doc) => {
    
    
    // Journals.find({rrn: "000033591420", terminalId: "20325U1X", MTI: "0200", STAN: "080309"} , (err, doc) => {
    
    console.log("file", doc)
    
    for(data of doc) {
    
    
    
    if(doc !== null) {
    
    
    const elnexunotifier = new ElnexuNotifier(notificationService, data);
    
    
    elnexunotifier.sendNotification();
    
    }
    
    
    }
    
    //}
    
    
    
    
    })
    
    
    //}
    
    
    }


const generateManualReport = async () => {

    try {
        await connectDatabase();
        // await  C24EmailReport()
        await generateC24Report();
        // await generateEtranzactReport();
        // await generateMikroReport();
        // await generateFlutterReport()
        // await sendManualPFMNotification()
        // await sendManualElnexuNotifier()
        
    } catch (error) {
        console.log("logs", error);
    }

//    await sendManualPFMNotification();



//    const file = await prepareReportFromIMS();

//    console.log(file);


//     //const file = 'Report/transaction-report-Sun Oct 14 2020.xlsx';


    // await flutterEmailReport(file).then((data) => { 
    //     Util.fileDataLogger("Email-Cronservice", `txn Report sent at ${new Date().toString()}`);
    //     console.log(data)
    // } ).catch((e)=> {
    //     console.log(e);
    // });

    // await generateC24Report();
     // const service = new journalService(unpackedMessage, unpackedHandlingServerMessage);

     // console.log(await service.updateWriteError(testtransaction));

//     //    await service.saveTransaction('NIBSS EPMS');

//     //    await service.updateSavedTransaction('NIBSS EPMS');

//     //    let msg = Util.getNibssResponseMessageFromCode("100");

//     // await service.updateNoResponseTransaction("100", msg);


   
//     // let reversalModel;

//     //    service.SaveReversalRequest(unpackedMessage, "NIBSS EPMS").then(async (res) => {

//     //     console.log(`Processing NIBSS Reversal at ${new Date().toString()}`);
//     //     // console.log(res);
//     //     reversalModel = res;
        

//     //    console.log(reversalModel);

//     //     if(reversalModel) {

//     //             reversalModel.messageReason = Util.getNibssResponseMessageFromCode(unpackedHandlingServerMessage.dataElements[39]),
//     //             reversalModel.responseCode = unpackedHandlingServerMessage.dataElements[39],
//     //             reversalModel.authCode = unpackedHandlingServerMessage.dataElements[38],
//     //             reversalModel.handlerResponseTime = new Date
            
//     //             const reversalResponse = await service.updateReversalResponse(reversalModel);

//     //             if(!reversalResponse) {

//     //                 console.error(`Error updating reversal`)

//     //             } else {

//     //                 console.log('Reversal before fail-over completed');
//     //                 console.log(reversalModel);

//     //             }

                
//     //     }

//     //     }).catch(err => { console.error(`Error saving reversal request data: ${err.toString()}`); });



//     // const tamsRrn = "29947473625240";

//     // service.SaveTamsReversal(unpackedMessage,tamsRrn,reversalResponse).then((res) => {
//     //     if (!res)
//     //         console.error(`Error saving TAMS reversal response data: ${err.toString()}`)
//     //     else {
//     //         console.log('TAMS reversal completed');
//     //         console.log(res);
//     //     }
//     // }).catch(err => console.error(`Error saving TAMS reversal response data: ${err.toString()}`))



//     // await service.updateWriteError(testtransaction);


    
}


const testSQLQueries = async () => {

    const terminalservice = new terminalService();


//     const transactionService = new JournalServices();

//     const journals = await transactionService.getTransactionByDate("issuerFeeSettled");

//     console.log(journals)
    const service = new bankconfigServices();

    // service.getConfig("2070T009", "POSVAS")

    const terminalObject = {
        //id: 1,
        terminalId: '2UP1T009',
        // createdAt: new Date(),
        // updatedAt: new Date(),
        masterKey_1: "4003FB50D3BD1D2BB1A39CBCC1576C35",
        masterKey_2: "4003FB50D3BD1D2BB1A39CBCC1576C35",
        pinKey_1: "A2106901081FDFCB9ADFFE9E0928DEC5",
        pinKey_2: "A2106901081FDFCB9ADFFE9E0928DEC5",
        sessionKey_1: "A2106901081FDFCB9ADFFE9E0928DEC5",
        sessionKey_2: "A2106901081FDFCB9ADFFE9E0928DEC5",
        batchNo: 1,
        sequenceNumber: 0,
        masterKey_tams: "732d1b15fbc0b6befc9b1437afd508b9",
        sessionKey_tams0: "0938c537b266744e7a35aadc90cac865",
        sessionKey_tams1: "0938c537b266744e7a35aadc90cac865",
        sessionKey_tams2: "0938c537b266744e7a35aadc90cac865",
        mechantID_tams: "5672",
        countryCode_tams: "566",
        debitAccountNumber: null,
        debitAccountCCY: null,
        debitAccountBranch: null,
        creditAccountNumber: null,
        creditAccountCCY: null,
        creditAccountBranch: null,
        moduleName: null
      }

    //await terminalservice.createTerminalKey(terminalObject, 'tams');

    await terminalservice.getSequenceNumber("2UP1T009")

    // terminalservice.findTerminal("2UP1T008")


//         // console.log(await service.getmerchantMscConfiguration("2011I3138"));

// //     // await addTerminalForNotification("5d31b9f577e6724950d06cfb");

// //     //await connectDatabase();

// //     //await updateBankConfigToBandA();

// //     //console.log(service.getSequenceNumber("2UP1T008"));


}

const binaryNumber = (N) => {

    let temp = N;
    let binary = "";

    while(temp > 0) {

        if(temp % 2 == 0) {

            binary = "0" + binary

        } else {
            
            binary = "1" + binary;

        }

        temp = Math.floor(temp / 2);

    }

    return binary;

}

// const crypto = require("crypto");
//     // do sha256 of the data with key
// function doSha256(key, data) {
//     let k = Buffer.from(key,'hex');
//     let hash = crypto.createHash('sha1');
//     hash.update(k)
//     hash.update(data);
//     return hash.digest('hex');
// }



// function doSHA512(toHash) {

//     let hash = crypto.createHash('sha512');

//     hash.update(toHash);

//     return hash.digest('hex');
// }

// console.log(doSHA512(`:COEL202019111`))

// console.log(doSha256("97f4cba663684a59e4cbbaf042274223", "97f4cba663684a59e4cbbaf042274223"))

// testSQLQueries();


// Map {
//     '9F26' => '74404C9810ECF330',
//     '9F27' => '80',
//     '9F10' => '0FA501A00200100000000000000000000F010000000000000000000000000000',
//     '9F37' => 'B847541C',
//     '9F36' => '02D3',
//     '95' => '0200240800',
//     '9A' => '201120',
//     '9C' => '00',
//     '9F02' => '000000002500',
//     '5F2A' => '0566',
//     '82' => '5800',
//     '9F1A' => '0566',
//     '9F34' => '420300',
//     '9F33' => 'E0F8C8',
//     '9F35' => '22',
//     '9F1E' => '3438333937363737',
//     '84' => 'A0000003710001',
//     '9F09' => '3F00',
//     '9F03' => '000000000000',
//     '5F34' => '01'
//   }

// AKINBINUADE

// tag: 9F26, data: 8A0A2A71879151F5
// tag: 9F27, data: 80
// tag: 9F10, data: 06010A03A0A803
// tag: 9F37, data: 606863B1
// tag: 9F36, data: 012F
// tag: 95, data: 0080048000
// tag: 9A, data: 201120
// tag: 9C, data: 00
// tag: 9F02, data: 000000010100
// tag: 5F2A, data: 0566
// tag: 82, data: 3C00
// tag: 9F1A, data: 0566
// tag: 9F34, data: 420300
// tag: 9F33, data: E0D0C8
// tag: 9F35, data: 22
// tag: 9F1E, data: 
// tag: 84, data: A0000000031010
// tag: 9F09, data: 008D
// tag: 9F03, data: 000000000000
// tag: 5F34, data: 01




// 9F260874404C9810ECF3309F2701809F10200FA501A00200100000000000000000000F0100000000000000000000000000009F3704B847541C9F360202D3950502002408009A032011209C01009F02060000000025005F2A020566820258009F1A0205669F34034203009F3303E0F8C89F3501229F1E0834383339373637378407A00000037100019F09023F009F03060000000000005F340101



// 9F260854FD4C7637E3DF8B9F2701809F100706010A03A0A8039F3704D62DFFF99F36020137950500800480009A032011259C01009F02060000000101005F2A02056682023C009F1A0205669F34034203009F3303E0D0C89F3501229F1E0836473432393336378407A00000000310109F0902008D9F03060000000000005F3401019F0702FF809F080200968F0108



//nibssICC:  9F26088A0A2A71879151F59F2701809F100706010A03A0A8039F3704606863B19F3602012F950500800480009A032011209C01009F02060000000101005F2A02056682023C009F1A0205669F34034203009F3303E0D0C89F3501229F1E008407A00000000310109F0902008D9F03060000000000005F3401019F0702FF809F080200968F0108


// nibssICC failing:  9F260854FD4C7637E3DF8B9F2701809F100706010A03A0A8039F3704D62DFFF99F36020137950500800480009A032011259C01009F02060000000101005F2A02056682023C009F1A0205669F34034203009F3303E0D0C89F3501229F1E0836473432393336378407A00000000310109F0902008D9F03060000000000005F3401019F0702FF809F080200968F0108

// console.log(Util.getICCData("9F260874404C9810ECF3309F2701809F10200FA501A00200100000000000000000000F0100000000000000000000000000009F3704B847541C9F360202D3950502002408009A032011209C01009F02060000000025005F2A020566820258009F1A0205669F34034203009F3303E0F8C89F3501229F1E0834383339373637378407A00000037100019F09023F009F03060000000000005F340101"))


// const doMSCCalculation = (amount) => {

//     let msc = {
//         local_msc_cap:1000,
//         local_msc: 0.5
//     }

//     if(!msc.local_msc && !msc.local_msc_cap) return;

//     let mscPercentage = parseFloat(msc.local_msc/100).toFixed(2)

//     let actualAmount = parseFloat(amount/100).toFixed(2);

//     console.log("actualAmount ", actualAmount);

//     let msCharge = parseFloat(mscPercentage * actualAmount).toFixed(2);

//     if(msCharge >  msc.local_msc_cap){
//         msCharge =  msc.local_msc_cap;
//     }

//     let totalVAT = parseFloat((0.75/100) * (msCharge)).toFixed(2)

//     console.log("totalVAT ", totalVAT);

//     let feeSuspence = Number(msCharge) + Number(totalVAT);

//     let amountToSettle = Number(actualAmount) - feeSuspence;

//     console.log("mscCharge: ", msCharge);
//     console.log("amountToSettleMerchant: ", amountToSettle);
//     console.log("feeSuspence: ", feeSuspence);

//     let acquirerFee = Number(parseFloat((12.5/100) * feeSuspence).toFixed(2));

//     let nibssFee = Number(parseFloat((7.5/100) * feeSuspence).toFixed(2));
//     let issuerFee = Number(parseFloat((30/100) * feeSuspence).toFixed(2));
//     let ptspFee = Number(parseFloat((25/100) * feeSuspence).toFixed(2));
//     let terminalOwnerFee = Number(parseFloat((25/100) * feeSuspence).toFixed(2));

//     let terminal50 = Number(parseFloat((50/100) * feeSuspence).toFixed(2));


//     console.log("acquirerFee: ", acquirerFee);
//     console.log("nibssFee: ", nibssFee);
//     console.log("issuerFee: ", issuerFee);
//     console.log("ptspFee: ", ptspFee);
//     console.log("terminalOwnerFee: ", terminalOwnerFee);

//     console.log(acquirerFee + nibssFee + issuerFee + ptspFee + terminalOwnerFee)
    





// // - AMOUNT DEBITED FROM CARD = 100naira
// // - FEE SUSPENCE (MSC + 7.5% of MSC)
// // - MERCHANT AMOUNT TO BE SETTLED (TRANSACTION AMOUNT - FEE SUSPENCE)

// // SHARING FEE SUSPENCE
// // - ACQUIRER (12.5% OF MSC (No VAT))
// // - NIBSS (7.5% OF MSC) (With VAT)
// // - PTSP (25% OF MSC) With VAT
// // - ISSUER (30% OF MSC) No VAT
// // - TERMINAL OWNER (25% OF MSC) WITH VAT
// // - VAT CREDITING












// }


//console.log("A10F6EF3C0356272867D135357F485E624A63C".substring(32, 38))


// 24A63C
generateManualReport();

// doMSCCalculation(105000)


