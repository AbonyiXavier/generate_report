'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('mw_transaction_journals', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rrn: {
        type: Sequelize.STRING
      },
      failOverRrn: {
        type: Sequelize.STRING
      },
      oldResCode: {
        type: Sequelize.STRING
      },
      responseCode: {
        type: Sequelize.STRING
      },
      onlinePin: {
        type: Sequelize.STRING
      },
      amount: {
        type: Sequelize.DECIMAL
      },
      currencyCode: {
        type: Sequelize.STRING
      },
      merchantName: {
        type: Sequelize.STRING
      },
      TVR: {
        type: Sequelize.STRING
      },
      CRIM: {
        type: Sequelize.STRING
      },
      merchantAddress: {
        type: Sequelize.STRING
      },
      merchantId: {
        type: Sequelize.STRING
      },
      terminalId: {
        type: Sequelize.STRING
      },
      STAN: {
        type: Sequelize.STRING
      },
      authCode: {
        type: Sequelize.STRING
      },
      transactionTime: {
        type: Sequelize.DATE
      },
      handlerResponseTime: {
        type: Sequelize.DATE
      },
      processTime: {
        type: Sequelize.DATE
      },
      merchantCategoryCode: {
        type: Sequelize.STRING
      },
      handlerName: {
        type: Sequelize.STRING
      },
      handler: {
        type: Sequelize.STRING
      },
      handlerUsed: {
        type: Sequelize.STRING
      },
      MTI: {
        type: Sequelize.STRING
      },
      maskedPan: {
        type: Sequelize.STRING
      },
      cardName: {
        type: Sequelize.STRING
      },
      cardExpiry: {
        type: Sequelize.STRING
      },
      processingCode: {
        type: Sequelize.STRING
      },
      messageReason: {
        type: Sequelize.STRING
      },
      script: {
        type: Sequelize.STRING
      },
      originalDataElements: {
        type: Sequelize.STRING
      },
      customerRef: {
        type: Sequelize.STRING
      },
      FIIC: {
        type: Sequelize.STRING
      },
      receiptSent: {
        type: Sequelize.BOOLEAN
      },
      receipt: {
        type: Sequelize.STRING
      },
      notified: {
        type: Sequelize.STRING
      },
      write2pos: {
        type: Sequelize.STRING
      },
      tamsRRN: {
        type: Sequelize.STRING
      },
      tamsBatchNo: {
        type: Sequelize.STRING
      },
      tamsTransNo: {
        type: Sequelize.STRING
      },
      tamsStatus: {
        type: Sequelize.STRING
      },
      tamsMessage: {
        type: Sequelize.STRING
      },
      interSwitchResponse: {
        type: Sequelize.STRING
      },
      // settledResponse: {
      //   type: Sequelize.STRING
      // },
      // settledAmount: {
      //   type: Sequelize.STRING
      // },
      // settled: {
      //   type: Sequelize.BOOLEAN
      // },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('mw_transaction_journals');
  }
};