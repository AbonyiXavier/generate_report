'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('terminal_nibss_keys', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      terminal_key_id: {
        type: Sequelize.STRING
      },
      master_key_1: {
        type: Sequelize.STRING
      },
      master_key_2: {
        type: Sequelize.STRING
      },
      pin_key_1: {
        type: Sequelize.STRING
      },
      pin_key_2: {
        type: Sequelize.STRING
      },
      session_key_1: {
        type: Sequelize.STRING
      },
      sessionKey_2: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('terminal_nibss_keys');
  }
};