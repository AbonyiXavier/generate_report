'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('terminal_tams_keys', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      terminal_key_id: {
        type: Sequelize.STRING
      },
      batch_no: {
        type: Sequelize.INTEGER
      },
      sequence_number: {
        type: Sequelize.INTEGER
      },
      master_key_tams: {
        type: Sequelize.STRING
      },
      session_key_tams0: {
        type: Sequelize.STRING
      },
      session_key_tams1: {
        type: Sequelize.STRING
      },
      session_key_tams2: {
        type: Sequelize.STRING
      },
      merchant_id_tams: {
        type: Sequelize.STRING
      },
      country_code_tams: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('terminal_tams_keys');
  }
};