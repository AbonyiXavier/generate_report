'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('bank_configs', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bank_name: {
        type: Sequelize.STRING
      },
      handler: {
        type: Sequelize.STRING
      },
      use_nibss_1: {
        type: Sequelize.BOOLEAN
      },
      use_nibss_2: {
        type: Sequelize.BOOLEAN
      },
      use_tams: {
        type: Sequelize.BOOLEAN
      },
      use_interswicth: {
        type: Sequelize.BOOLEAN
      },
      use_upsl: {
        type: Sequelize.BOOLEAN
      },
      tam_priority: {
        type: Sequelize.BOOLEAN
      },
      no_attempt_nibss: {
        type: Sequelize.BOOLEAN
      },
      use_selected: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('bank_configs');
  }
};