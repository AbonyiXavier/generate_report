'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('bank_config_tams', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      bank_config_id: {
        type: Sequelize.INTEGER
      },
      ip_live: {
        type: Sequelize.STRING
      },
      port_live: {
        type: Sequelize.STRING
      },
      ip_test: {
        type: Sequelize.STRING
      },
      port_test: {
        type: Sequelize.STRING
      },
      com_key_1: {
        type: Sequelize.STRING
      },
      com_key_2: {
        type: Sequelize.STRING
      },
      bdk_name: {
        type: Sequelize.STRING
      },
      tam_direct: {
        type: Sequelize.BOOLEAN
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('bank_config_tams');
  }
};