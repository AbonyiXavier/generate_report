'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     **/
     await queryInterface.bulkInsert('transaction_journals', [     {
      "receipt": "paper",
      "receipt_sent": false,
      "rrn": "000603105909",
      "online_pin": true,
      "merchant_name": "ONYESCOM VENTURES LTD ",
      "merchant_address": "KD           LANG",
      "merchant_id": "FBP205600444741",
      "terminal_id": "2071GAK4",
      "stan": "105235",
      "transaction_time": "2020-09-17T09:51:34.400Z",
      "merchantCategoryCode": "5621",
      //"handler_name": "NIBSS POSVAS",
      "mti": "0200",
      "masked_pan": "533477XXXXXX4549",
      "processing_code": "000000",
      "amount": 100,
      "currency_code": "566",
      "messageReason": "Approved",
      "original_data_elements": null,
      "customer_ref": "PAX|5C110793|7.8.17",
      "tvr": "0000240800",
      "crim": "660C333A8C9FF016",
      "FIIC": "557694",
      "auth_code": "779130",
      "failover_rrn": "",
      "handlerResponseTime": "2020-09-17T09:51:36.747Z",
      "handler_used": "POSVAS",
      "old_rescode": "",
      "responseCode": "00",
      "script": "9F360200AB910a2476EB460B68373C0012",
      "tams_batch_no": "",
      "tams_message": "",
      "tams_rrn": "",
      "tams_status": "",
      "tams_transaction_no": "",
      "write_to_pos": "00",
      "createdAt": new Date(),
      "updatedAt": new Date()
     },
     {
      "receipt": "paper",
      "receipt_sent": false,
      "rrn": "000603105345",
      "online_pin": true,
      "merchant_name": "ONYESCOM VENTURES LTD ",
      "merchant_address": "KD           LANG",
      "merchant_id": "FBP205600444741",
      "terminal_id": "2071GAK4",
      "stan": "105345",
      "transaction_time": "2020-09-17T09:51:34.400Z",
      "merchantCategoryCode": "5621",
      //"handler_name": "NIBSS POSVAS",
      "mti": "0200",
      "masked_pan": "533477XXXXXX4549",
      "processing_code": "000000",
      "amount": 100,
      "currency_code": "566",
      "messageReason": "Approved",
      "original_data_elements": null,
      "customer_ref": "PAX|5C110793|7.8.17",
      "tvr": "0000240800",
      "crim": "660C333A8C9FF016",
      "FIIC": "557694",
      "auth_code": "779130",
      "failover_rrn": "",
      "handlerResponseTime": "2020-09-17T09:51:36.747Z",
      "handler_used": "POSVAS",
      "old_rescode": "",
      "responseCode": "00",
      "script": "9F360200AB910a2476EB460B68373C0012",
      "tams_batch_no": "",
      "tams_message": "",
      "tams_rrn": "",
      "tams_status": "",
      "tams_transaction_no": "",
      "write_to_pos": "00",
      "createdAt": new Date(),
      "updatedAt": new Date()
     },
   
     ], {});
    


  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     **/
     await queryInterface.bulkDelete('transaction_journals', null, {});
     
  }
};
