'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_selectors extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      
    }
  };
  bank_selectors.init({
    bank_config_id: DataTypes.INTEGER,
    selector: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'bank_selectors',
  });
  return bank_selectors;
};