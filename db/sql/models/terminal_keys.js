'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class terminal_keys extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      // terminal_keys.hasOne(models.terminal_keys, {
      //   foreignKey: 'terminal_key_id',
      //   as: 'terminal_keys',
      // });
      
    }
  };


  terminal_keys.init({
      terminal_id: DataTypes.STRING
    }, {
      sequelize,
      tableName: 'mw_terminal_keys',
      modelName: 'terminal_keys',
    });

    return terminal_keys;
};