'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class transaction_journals extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };

  
  transaction_journals.init({
    rrn: DataTypes.STRING,
    failOverRrn: DataTypes.STRING,
    oldResCode: DataTypes.STRING,
    responseCode: DataTypes.STRING,
    onlinePin: DataTypes.STRING,
    amount: DataTypes.DECIMAL,
    currencyCode: DataTypes.STRING,
    merchantName: DataTypes.STRING,
    TVR: DataTypes.STRING,
    CRIM: DataTypes.STRING,
    merchantAddress: DataTypes.STRING,
    merchantId: DataTypes.STRING,
    terminalId: DataTypes.STRING,
    STAN: DataTypes.STRING,
    authCode: DataTypes.STRING,
    transactionTime: DataTypes.DATE,
    handlerResponseTime: DataTypes.DATE,
    processTime: DataTypes.DATE,
    merchantCategoryCode: DataTypes.STRING,
    handlerName: DataTypes.STRING,
    handler: DataTypes.STRING,
    handlerUsed: DataTypes.STRING,
    MTI: DataTypes.STRING,
    maskedPan: DataTypes.STRING,
    FIIC: DataTypes.STRING,
    cardName: DataTypes.STRING,
    cardExpiry: DataTypes.STRING,
    processingCode: DataTypes.STRING,
    messageReason: DataTypes.STRING,
    script: DataTypes.STRING,
    originalDataElements: DataTypes.STRING,
    customerRef: DataTypes.STRING,
    notified: DataTypes.STRING,
    write2pos: DataTypes.STRING,
    tamsRRN: DataTypes.STRING,
    tamsBatchNo: DataTypes.STRING,
    tamsTransNo: DataTypes.STRING,
    tamsStatus: DataTypes.STRING,
    tamsMessage: DataTypes.STRING,
    interSwitchResponse: DataTypes.STRING,
    //settledResponse: DataTypes.STRING,
    //settledAmount: DataTypes.STRING,
    receiptSent: DataTypes.BOOLEAN,
    receipt: DataTypes.STRING,
    //settled: DataTypes.BOOLEAN
  }, {
    sequelize,
    tableName: 'mw_transaction_journals',
    modelName: 'transaction_journals',
  });
  return transaction_journals;
};