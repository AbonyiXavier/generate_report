'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class terminal_tams_keys extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    terminal_tams_keys.belongsTo(models.terminal_keys, {
        foreignKey: 'terminal_key_id',
        as: 'terminal_keys',
      });
    }
  };
  terminal_tams_keys.init({
    terminal_key_id: DataTypes.STRING,
    batch_no: DataTypes.INTEGER,
    sequence_number: DataTypes.INTEGER,
    master_key_tams: DataTypes.STRING,
    session_key_tams0: DataTypes.STRING,
    session_key_tams1: DataTypes.STRING,
    session_key_tams2: DataTypes.STRING,
    merchant_id_tams: DataTypes.STRING,
    country_code_tams: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'terminal_tams_keys',
    tableName: 'mw_terminal_tams_keys'
  });
  return terminal_tams_keys;
};