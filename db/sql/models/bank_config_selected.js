'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_config_selected extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  bank_config_selected.init({
    bank_config_id: DataTypes.INTEGER,
    bank_selector_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'bank_config_selected',
  });
  return bank_config_selected;
};