'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_config_tams extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      bank_config_tams.belongsTo(models.bank_config, {
        foreignKey: 'bank_config_id',
        as: 'bank_config',
      });


    }
  };
  bank_config_tams.init({
    bank_config_id: DataTypes.INTEGER,
    ip_live: DataTypes.STRING,
    port_live: DataTypes.STRING,
    ip_test: DataTypes.STRING,
    port_test: DataTypes.STRING,
    com_key_1: DataTypes.STRING,
    com_key_2: DataTypes.STRING,
    bdk_name: DataTypes.STRING,
    tam_direct: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'bank_config_tams',
  });
  return bank_config_tams;
};