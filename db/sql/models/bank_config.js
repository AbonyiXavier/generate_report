'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class bank_config extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      bank_config.hasOne(models.bank_config_tams, {
        foreignKey: 'bank_config_id',
        as: 'bank_config_tams',
        onDelete: 'CASCADE'
      });

      bank_config.hasMany(models.bank_selectors, {
        foreignKey: 'bank_config_id',
        as: 'bank_selectors',
        onDelete: 'CASCADE'
      });
    }
  };
  bank_config.init({
    bank_name: DataTypes.STRING,
    handler: DataTypes.STRING,
    use_nibss_1: DataTypes.BOOLEAN,
    use_nibss_2: DataTypes.BOOLEAN,
    use_tams: DataTypes.BOOLEAN,
    use_interswicth: DataTypes.BOOLEAN,
    use_upsl: DataTypes.BOOLEAN,
    tam_priority: DataTypes.BOOLEAN,
    no_attempt_nibss: DataTypes.BOOLEAN,
    use_selected: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'bank_config',
  });
  return bank_config;
};