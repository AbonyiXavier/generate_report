'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class terminal__nibss_keys extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      terminal__nibss_keys.belongsTo(models.terminal_keys, {
          foreignKey: 'terminal_key_id',
          as: 'terminal_keys',
      });
    
    
    }
  };
  terminal__nibss_keys.init({
    terminal_key_id: DataTypes.STRING,
    master_key_1: DataTypes.STRING,
    master_key_2: DataTypes.STRING,
    pin_key_1: DataTypes.STRING,
    pin_key_2: DataTypes.STRING,
    session_key_1: DataTypes.STRING,
    sessionKey_2: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'terminal_nibss_keys',
    tableName: 'mw_terminal_nibss_keys',
    
  });
  return terminal__nibss_keys;
};