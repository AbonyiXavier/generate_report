const Sequelize = require("sequelize");
const IDatabase = require('../models').sequelize;
const Terminalkeys = require("../models").terminal_keys;
const TerminalNibsskeys = require("../models").terminal_nibss_keys;
const TerminalTamskeys = require("../models").terminal_tams_keys;
const Util = require("../../../helpers/Utils");

class terminalServices {

    async findTerminal(terminalId) {
        // const queryString = `SELECT * FROM mw_terminal_key WHERE mw_terminal_keys.terminal_id = '${terminalId}' ((INNER JOIN mw_terminal_nibss_keys ON mw_terminal_nibss_keys.terminal_key_id = mw_terminal_keys.id) INNER JOIN mw_terminal_tams_keys ON mw_terminal_tams_keys.terminal_key_id = mw_terminal_keys.id)`;

        const queryString =  `SELECT * FROM (( mw_terminal_keys 
                              INNER JOIN mw_terminal_nibss_keys ON mw_terminal_nibss_keys.terminal_key_id = mw_terminal_keys.id)
		                      INNER JOIN mw_terminal_tams_keys ON mw_terminal_tams_keys.terminal_key_id = mw_terminal_keys.id) 
		                      WHERE mw_terminal_keys.terminal_id = '${terminalId}'`

        return IDatabase.query(queryString, { type: Sequelize.QueryTypes.SELECT}).then(terminal => {
            
            //console.log(terminal[0] || null);

            return terminal[0] || null;
        });
    }

    async getSequenceNumber(terminalId){
        const terminal = await this.findTerminal(terminalId);
        let sequenceNumber = parseInt(terminal.sequence_number) || 0;
        sequenceNumber+=1;
        terminal.sequence_number = sequenceNumber;
        await TerminalTamskeys.update({ sequence_number: sequenceNumber },
            { where: { terminal_key_id : terminal.terminal_key_id } }).then(data => { 
            console.log(data);
        }).catch(err => { 
            console.error(`A error has occured here => ${err.toString()}`)
        });
        return sequenceNumber;
    }

    async createNibssKey(terminalNibssObj) {

        return await TerminalNibsskeys.create(terminalNibssObj).then((data) => {
            console.log(data.dataValues);
            if(data.dataValues) {
                return true;
            }
        }).catch((err) => {
            console.log(`There was an error creating terminal key record on db ${err}`);
            return false;
        });
    }

    async createTamsKey(terminalTamsObj) {

        return await TerminalTamskeys.create(terminalTamsObj).then((data) => {
            console.log(data.dataValues);
            if(data.dataValues) {
                return true;
            }
            return false;
        }).catch((err) => {
            console.log(`There was an error creating terminal key record on db ${err}`);
            return false;
        });

    }
    

    terminalTamsObj
    

    
    // async createOrUpdate(terminalId, update, done) {
    //     let query = {
    //         terminalId: terminalId
    //     };
    //     options = {
    //         upsert: true,
    //         new: true,
    //         setDefaultsOnInsert: true
    //     };
    
    //     // Find the document
    //     Model.findOneAndUpdate(query, update, options, function (error, result) {
    //         done(error, result);
    //     });
    // } 


    


}

module.exports = terminalServices;