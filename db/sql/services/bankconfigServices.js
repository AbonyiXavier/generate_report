const Sequelize = require("sequelize");
const IDatabase = require('../models').sequelize;
const Terminalkeys = require("../models").terminal_keys;
const TerminalNibsskeys = require("../models").terminal_nibss_keys;
const TerminalTamskeys = require("../models").terminal_tams_keys;
const Util = require("../../../helpers/Utils");


class BankConfigServices {

    async getConfig(terminalId, handlerUsed) {

        const selectors = terminalId.substr(0,4);
        const queryString =  `SELECT * FROM ((( mw_bank_configs
            INNER JOIN mw_bank_config_selecteds ON mw_bank_config_selecteds.bank_config_id = mw_bank_configs.id)
            INNER JOIN mw_bank_config_tams ON mw_bank_config_tams.bank_config_id = mw_bank_configs.id)
            INNER JOIN mw_bank_selectors ON mw_bank_selectors.bank_config_id = mw_bank_configs.id)
            WHERE mw_bank_configs.handler = '${handlerUsed}' AND mw_bank_selectors.selector = '${selectors}'`

        console.log(queryString);
    }


}

module.exports = BankConfigServices