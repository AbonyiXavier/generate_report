const Journals = require("../models").transaction_journals;
const Util = require("../../../helpers/Utils");

class JournalServices {

    constructor(unpackedMessage, unpackedHandlingServerMessage, handlerUsed = "NIBSS EPMS") {
        this.transactionDetails = {};

        this.unpackedMessage = unpackedMessage;

        this.handlerUsed = handlerUsed;

        this.unpackedHandlingServerMessage = unpackedHandlingServerMessage;

        this.handlingModelInstance = null;
    }

    async saveTransaction(handlerName) {

        let saveDetails = {
            rrn: this.unpackedMessage.dataElements[37],
            onlinePin: (this.unpackedMessage.dataElements[52] !== null ? true : false),
            merchantName: this.unpackedMessage.dataElements[43].substring(0, 22),
            merchantAddress: this.unpackedMessage.dataElements[43].substring(23),
            merchantId: this.unpackedMessage.dataElements[42],
            terminalId: this.unpackedMessage.dataElements[41],
            STAN: this.unpackedMessage.dataElements[11],
            transactionTime: new Date(),
            merchantCategoryCode: this.unpackedMessage.dataElements[18],
            handlerName: handlerName,
            MTI: this.unpackedMessage.mti,
            maskedPan: this.unpackedMessage.dataElements[2].substr(0, 6) + ''.padEnd(this.unpackedMessage.dataElements[2].length - 10, 'X') + this.unpackedMessage.dataElements[2].slice(-4),
            processingCode: this.unpackedMessage.dataElements[3],
            amount: parseInt(this.unpackedMessage.dataElements[4]),
            currencyCode: this.unpackedMessage.dataElements[49],
            messageReason: this.unpackedMessage.dataElements[56],
            originalDataElements: this.unpackedMessage.dataElements[90],
            customerRef: this.unpackedMessage.dataElements[59] || ""
        }


        if (Util.isMitType(this.unpackedMessage, '02')) {
            let iccData = Util.getICCData(this.unpackedMessage);
            saveDetails.TVR = iccData.get('95');
            saveDetails.CRIM = iccData.get('9F26');
        }

        this.transactionDetails = {
            ...this.transactionDetails,
            ...saveDetails
        };

        console.log(this.transactionDetails);

        this.handlingModelInstance = new Journals(this.transactionDetails);

        let saved = false;


        await this.handlingModelInstance.save().then(() => {

            console.log(`Saved Transaction from Terminal: ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}`);

            saved = true;

        })
        .catch((error) => {

            console.log(`Exception Saving ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}, Exception ${error}`);

        });
        

        return saved;

    }

    async updateSavedTransaction(handlerUsed) {
        let updateDetails = {
            messageReason: Util.getNibssResponseMessageFromCode(this.unpackedHandlingServerMessage.dataElements[39]),
            failOverRrn: Util.getFailOverRRN(this.unpackedMessage, this.unpackedHandlingServerMessage),
            oldResCode: this.transactionDetails.oldResCode ? this.transactionDetails.oldResCode : '',
            responseCode: this.unpackedHandlingServerMessage.dataElements[39],
            script: this.unpackedHandlingServerMessage.dataElements[55],
            authCode: this.unpackedHandlingServerMessage.dataElements[38] ? this.unpackedHandlingServerMessage.dataElements[38] : this.transactionDetails.authCode,
            handlerResponseTime: new Date,


            tamsBatchNo: this.transactionDetails.tamsBatchNo || "",
            tamsTransNo: this.transactionDetails.tamsTransNo || "",
            tamsStatus: this.transactionDetails.tamsStatus || "",
            tamsMessage: this.transactionDetails.tamsMessage || "",
            tamsRRN: this.transactionDetails.tamsRRN || "",

            handlerUsed: handlerUsed,
            interSwitchResponse: this.transactionDetails.interSwitchResponse || ''

        }

        // transactionDetails after process
        this.transactionDetails = {
            ...this.transactionDetails,
            ...updateDetails
        };

        let transactionID = this.handlingModelInstance.id;

        console.log(this.transactionDetails);

        let updated = false;

        await this.handlingModelInstance.set(updateDetails).save()
            .then(() => {

                console.log(`Updated Transaction from Terminal: ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}`);

                updated = true;

            })
            .catch((error) => {

                console.log(`Exception Updating ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}, Exception ${error}`);

            });

        return updated;

    }

    async updateNoResponseTransaction(resCode, messageReason) {
        let updateDetails = {
            handlerUsed: this.handlerUsed,
            responseCode: resCode,
            messageReason: messageReason
        }

        // transactionDetails after process
        this.transactionDetails = {
            ...this.transactionDetails,
            ...updateDetails
        };

        console.log(this.transactionDetails);

        let updated = false;

        await Journals.update( updateDetails, { where: {
                    terminalId: this.transactionDetails.terminalId,
                    rrn: this.transactionDetails.rrn,
                    STAN: this.transactionDetails.STAN,
                    maskedPan: Util.getMaskPan(this.unpackedMessage)
                }
            })
            .then(() => {

                console.log(`Updated Transaction from Terminal: ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}`);

                updated = true;

            })
            .catch((error) => {

                console.log(`Exception Updating ${this.transactionDetails.terminalId}, with RRN: ${this.transactionDetails.rrn}, Exception ${error}`);

            });

        return updated;

    }

    async SaveReversalRequest(unpackedMessage,handlerName) {

        let saveDetails = {
            rrn: unpackedMessage.dataElements[37],
            onlinePin: (unpackedMessage.dataElements[52] !== null ? true : false),
            merchantName: unpackedMessage.dataElements[43].substring(0, 22),
            merchantAddress: unpackedMessage.dataElements[43].substring(23),
            merchantId: unpackedMessage.dataElements[42],
            terminalId: unpackedMessage.dataElements[41],
            STAN: unpackedMessage.dataElements[11],
            transactionTime: new Date(),
            merchantCategoryCode: unpackedMessage.dataElements[18],
            handlerName: handlerName,
            MTI: unpackedMessage.mti,
            maskedPan: unpackedMessage.dataElements[2].substr(0, 6) + ''.padEnd(unpackedMessage.dataElements[2].length - 10, 'X') + unpackedMessage.dataElements[2].slice(-4),
            processingCode: unpackedMessage.dataElements[3],
            amount: parseInt(unpackedMessage.dataElements[4]),
            currencyCode: unpackedMessage.dataElements[49],
            messageReason: unpackedMessage.dataElements[56],
            originalDataElements: unpackedMessage.dataElements[90]
        }

        return await Journals.create(saveDetails).then((data) => {
            console.log(data.dataValues);
            return data.dataValues;
        }).catch(err => { 
            console.error(`Error saving Reversal response data: ${err.toString()}`)
            return false; })

    }

    async updateReversalResponse(transaction) {

        return await Journals.update(transaction, { where: {
            id: transaction.id
        }}).then(data => { 
            console.log(data);
            return data;
        }).catch(err => { 
            console.error(`Error updating reversal response data: ${err.toString()}`)
            return false
        });

    }


    async SaveTamsReversal(unpackedMessage,originalSN,reversalResponse){
        let saveDetails = {
            rrn: originalSN,
            onlinePin: (unpackedMessage.dataElements[52] !== null ? true : false),
            merchantName: unpackedMessage.dataElements[43].substring(0, 22),
            merchantAddress: unpackedMessage.dataElements[43].substring(23),
            merchantId: unpackedMessage.dataElements[42],
            terminalId: unpackedMessage.dataElements[41],
            STAN: unpackedMessage.dataElements[11],
            transactionTime: new Date(),
            merchantCategoryCode: unpackedMessage.dataElements[18],
            handlerName: "TAMS",
            handlerUsed: "TAMS",
            MTI: "TAMS Reversal 4",
            maskedPan: unpackedMessage.dataElements[2].substr(0, 6) + ''.padEnd(unpackedMessage.dataElements[2].length - 10, 'X') + unpackedMessage.dataElements[2].slice(-4),
            processingCode: "000000",
            amount: parseInt(unpackedMessage.dataElements[4]),
            currencyCode: unpackedMessage.dataElements[49],
    
            tamsBatchNo: reversalResponse.batchNo || "",
            tamsTransNo: reversalResponse.tranNo || "",
            tamsStatus:  reversalResponse.status || "",
            tamsMessage: reversalResponse.message || "",
            tamsRRN :   reversalResponse.rrn || ""
        }

        return await Journals.create(saveDetails).then((data) => {
            console.log(data.dataValues);
            return data.dataValues;
        }).catch(err => { 
            console.error(`Error saving TAMS reversal response data: ${err.toString()}`)
            return false; })


    }


    async SaveInterswitchReversal(unpackedMessage,reversalResponse){
        let saveDetails = {
    
            rrn: unpackedMessage.dataElements[37],
            onlinePin: (unpackedMessage.dataElements[52] !== null ? true : false),
            merchantName: unpackedMessage.dataElements[43].substring(0, 22),
            merchantAddress: unpackedMessage.dataElements[43].substring(23),
            merchantId: unpackedMessage.dataElements[42],
            terminalId: unpackedMessage.dataElements[41],
            STAN: unpackedMessage.dataElements[11],
            transactionTime: new Date(),
            merchantCategoryCode: unpackedMessage.dataElements[18],
            handlerName: "INTERSWITCH",
            handlerUsed: "INTERSWITCH",
            MTI: "0420",
            maskedPan: unpackedMessage.dataElements[2].substr(0, 6) + ''.padEnd(unpackedMessage.dataElements[2].length - 10, 'X') + unpackedMessage.dataElements[2].slice(-4),
            processingCode: "000000",
            amount: parseInt(unpackedMessage.dataElements[4]),
            currencyCode: unpackedMessage.dataElements[49],
            responseCode : reversalResponse.resCode
        }

        return await Journals.create(saveDetails).then((data) => {
            console.log(data.dataValues);
            return data.dataValues;
        }).catch(err => { 
            console.error(`Error saving Interswitch reversal response data: ${err.toString()}`)
            return false; })
    }


    async updateWriteError(transaction){

       let updated = false;

       await Journals.findOne({ where: 
            {rrn : transaction.rrn, terminalId : transaction.terminalId, STAN : transaction.STAN },
            order: [['id', 'DESC']],
       }).then((data) => {

            if(data !== null) {

                updated = true;

                data.update({ write2pos : '06' });

            }

            console.error(`Could not find transaction while updating writ2pos data, terminalId: ${transaction.terminalId}, rrn: ${transaction.rrn} at ${new Date().toString()}`);

       }).catch((err) => {

                console.error(`Error updating writ2pos data, terminalId: ${transaction.terminalId}, rrn: ${transaction.rrn} at ${new Date().toString()}, Response ${err.toString()}`);
    
        });
        
        return updated;
    }
    

}

module.exports = JournalServices;