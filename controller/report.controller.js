
    const Report = require("../model/report.model")
    const { generateFlutterReport } = require("../services/report.service")

    module.exports = {
        flutterwaveReport: async(request, response) => {
            try {
                // const { startDate, endDate } = request.body;
                // console.log("request");

                const data = await generateFlutterReport();

                return response.json(data)
            } catch (error) {
                console.log(error);
            }
        },
    }