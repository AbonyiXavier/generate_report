const mongoose = require("mongoose");


const connectDatabase = async () => {

    const {
        databaseDriver,
        databaseHost,
        databasePort,
        databaseUser,
        databasePwd,
        databaseCollection,
        databaseHost_2,
        databasePort_2,
        databaseHost_3,
        databasePort_3,
        replicaSet
    } = {

        databaseDriver: process.env.DATABASE_DRIVER || 'mongodb',
        databaseHost: process.env.DATABASE_HOST || 'localhost',
        databasePort: process.env.DATABASE_PORT || '27017',
        databaseUser: process.env.DATABASE_USER || 'eft-user',
        databasePwd: process.env.DATABASE_PWD || '4839!!Itex',
        databaseCollection: process.env.DATABASE_COLLECTION || 'eftEngine',

        databaseHost_2: process.env.DATABASE_HOST_2 || 'localhost',
        databasePort_2: process.env.DATABASE_PORT_2 || '27017',

        databaseHost_3: process.env.DATABASE_HOST_3 || 'localhost',
        databasePort_3: process.env.DATABASE_PORT_3 || '27017',
        replicaSet: process.env.REPLICA_SET || 'rs0'

    }

    if (process.env.APP_ENV == "local" && process.env.APP_DEBUG) {

        console.log(`Database Connection: ${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });

    }

    console.log(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`)

    mongoose.Promise = global.Promise;
    //mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort},${databaseHost_2}:${databasePort_2},${databaseHost_3}:${databasePort_3}/${databaseCollection}`, {
    mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}`, {
    // mongoose.connect(`${databaseDriver}://${databaseUser}:${databasePwd}@${databaseHost}:${databasePort}/${databaseCollection}?authSource=admin`, {
        useNewUrlParser: true,
        // sets how many times to try reconnecting
        reconnectTries: Number.MAX_VALUE,
        // sets the delay between every retry (milliseconds)
        reconnectInterval: 1000,
        autoReconnect : true,
        numberOfRetries : 5,
        keepAlive: 300000, connectTimeoutMS: 300000,
        // replset name
        replicaSet,
        readPreference: 'secondaryPreferred',
        poolSize:process.env.POOL_SIZE || 8
         
    },()=>{
        // rs.secondaryOk()
        console.log("Connected now")
        // this.setUpEmailCron();

        // this.setUpDataSocket();
    });

}

module.exports = connectDatabase()
