const express = require("express");
const logger = require("morgan");
const cors = require("cors");
const createError = require("http-errors");
const dotenv = require("dotenv");
const router = require("./routes/index");


require("./connect/database");

dotenv.config();

const app = express();

app.use(cors());
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/api", router);

app.get("/", (request, response) => {
  response.status(200).json({
    status: true,
    message: "Report API",
  });
});

app.use((request, response, next) => {
  next(createError.NotFound());
});

app.use((err, request, response, next) => {
  response.status(err.status || 500);
  response.send({
    error: {
      status: err.status || 500,
      message: err.message,
    },
  });
});

const PORT = process.env.PORT || 443;


app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}`);
});

module.exports = app;