const mongoose = require("mongoose");

let registeredNotification = new mongoose.Schema({

    name: {
        type: String,
        unique: true
    },
    merchantId: {
        type: String,
        default: ''
    },
    terminalId:  {
        type: String,
        default: ''
    },
    identifier : {
        type : String,
        default : ''
    },
    notificationService: String,
    enabled: Boolean,
    selectors : [String]
});

module.exports = registeredNotification;