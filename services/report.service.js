require("dotenv").config();
const mongoose = require('mongoose');
const data = require("../data");
const morgan = require('morgan');
const PFMExternalNotifier = require("../pfmexternalnotifier")
const ElnexuNotifier = require("../elnexunotifier")
const nodemailer = require("nodemailer");
const json2xls = require('json2xls');
const moment = require('moment-timezone');
const fs = require('fs');
const path = require('path');
const Journals = require('../model/journamodel');
const registeredNotificationModel = require("../model/registerednotificationmodel");
const ETZNotifier = require("../etznotifier");



const generateFlutterReport = async () => {

    let option = {
        fields: [
            "rrn", "merchantName", "merchantAddress", "merchantId",
            "terminalId", "STAN", "transactionTime","handlerResponseTime" ,"merchantCategoryCode",
            "MTI", "maskedPan", "processingCode", "amount", "messageReason",
            "responseCode", "authCode","notified"
        ]
    };

        let startDate = moment('20211008', 'YYYYMMDD').startOf('day').toDate();
        let endDate = moment('20211008', 'YYYYMMDD').endOf('day').toDate();
      
        // // for live

        // let startDate = moment().startOf('day').toDate();

        // let endDate = moment().endOf('day').toDate();

        let uniqueKey = Math.random().toString(36).slice(2)


        // console.log(endDate)

        console.log("Here now!")
        let report = await Journals.find(
        {
            responseCode : "00",
            customerRef : {$regex : /^flutter/ },transactionTime: {
            $gte: startDate,
            $lt: endDate
        }}, option.fields).sort('-terminalId');

        console.log("report", report);

        
        // let terminalId = "2215166A"
        // let rrn = "201827111947"
        // const report = await Journals.find(
        //     {
        //         // terminalId, 
        //         // rrn,
        //     transactionTime: {
        //         $gte: startDate,
        //         $lt: endDate
        //     }
        // }, option.fields).sort('-terminalId');

        console.log(`Transaction found: ${report.length} at ${new Date().toString()}`);

        if (report.length <= 0)
            return false;

        let xls = json2xls(report, option);

        let date = new Date().toDateString();

        let filesPath = `Report/flutter-transaction-report-${uniqueKey}.xlsx`;
        // let filesPath = `Report/flutter-transaction-report-${date}.xlsx`;
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }
        fs.writeFileSync(filesPath, xls, 'binary');
        return filesPath;
}

module.exports = {
    generateFlutterReport
}