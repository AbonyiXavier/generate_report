const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema(
  {
    startDate: {  type : Date },
    endDate: {  type : Date },
  },
  {
    timestamps: true,
  }
);

const ReportModel = mongoose.model("Report", reportSchema);

module.exports = ReportModel;
