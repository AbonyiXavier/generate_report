const mongoose = require("mongoose");

const journalSchema = require("../schema/journalschema");

let journalModel = mongoose.model('Journal', journalSchema);

module.exports = journalModel;