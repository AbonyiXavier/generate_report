const { Router } = require("express");
const { flutterwaveReport } = require("../controller/report.controller")
const router = new Router();


router.get("/flutterwave", flutterwaveReport);

module.exports = router;
