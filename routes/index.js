const { Router } = require("express");
const reportRoute = require("./report.routes");

const router = new Router();

router.use("/v1", reportRoute);

module.exports = router;
