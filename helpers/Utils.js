const SimpleNodeLogger = require('simple-node-logger');
const path = require('path');
const fs = require('fs');
const crypto = require("crypto");
const cryptojs = require("crypto-js");
const bankBins = require("./bank-config");
const TlvTags =  require('../config/tvlTags.json');


module.exports = {
    failoverResponses:  ['06','96'],
    keysProcessingCodes : ['9A', '9B', '9G'],
    keysExchangeProcessingCodes : ['9A', '9B', '9G', '9C'],
    TmkProcessingCode : '9A',
    TpkProcessingCode : '9G',
    SkProcessingCode : '9B',
    GParamProcessingCode : '9C',
    CallHomeProcessingCode : '9D',
    configDataTags : [
        //Merchant Id 
        '03015' ,
        //Merchant's name
        '52040',
        //Currency Code
        '05003',
        //Country Code
        '06003',
        //Merchant Category Code
        '08004'
    ],
    handlers : {
        nibss1 : process.env.handler,
        nibss2  : process.env.handler_2,
        tams : process.env.handler_tams, //fund end processor
        interswitch : process.env.handler_interswitch
    },

     /**
      * check iso message request type
      * @param {Object} unpackedMessage unpacked iso message
      * @param {String} msgType message type to check for
      * @returns {Boolean} 
      */
    isMitType(unpackedMessage,msgType){
        return unpackedMessage.mti.substring(0, 2) == msgType;
    },

    /**
     * extract response code from the unpacked message dataElements
     * @param {Object} unpackedMessage unpacked iso  message
     * @returns {String} response code
     */
    getResponseCode(unpackedMessage){
        return unpackedMessage.dataElements[39];
    },

        /**
     * map response code from nibss to it's message
     * @param {String} code response code from nibss
     */
    getNibssResponseMessageFromCode(code) {
        switch (code) {
            case "00":
                return "Approved";
            case "01":
                return "Refer to card issuer";
            case "02":
                return "Refer to card issuer, special condition";
            case "03":
                return "Invalid merchant";
            case "04":
                return "Pick-up card";
            case "05":
                return "Do not honor";
            case "06":
                return "Error";
            case "07":
                return "Pick-up card, special condition";
            case "08":
                return "Honor with identification";
            case "09":
                return "Request in progress";
            case "10":
                return "Approved,partial";
            case "11":
                return "Approved,VIP";
            case "12":
                return "Invalid transaction";
            case "13":
                return "Invalid amount";
            case "14":
                return "Invalid card number";
            case "15":
                return "No such issuer";
            case "16":
                return "Approved,update track 3";
            case "17":
                return "Customer cancellation";
            case "18":
                return "Customer dispute";
            case "19":
                return "Re-enter transaction";
            case "20":
                return "Invalid response";
            case "21":
                return "No action taken";
            case "22":
                return "Suspected malfunction";
            case "23":
                return "Unacceptable transaction fee";
            case "24":
                return "File update not supported";
            case "25":
                return "Unable to locate record";
            case "26":
                return "Duplicate record";
            case "27":
                return "File update edit error";
            case "28":
                return "File update file locked";
            case "29":
                return "File update failed";
            case "30":
                return "Format error";
            case "31":
                return "Bank not supported";
            case "32":
                return "Completed, partially";
            case "33":
                return "Expired card, pick-up";
            case "34":
                return "Suspected fraud, pick-up";
            case "35":
                return "Contact acquirer, pick-up";
            case "36":
                return "Restricted card, pick-up";
            case "37":
                return "Call acquirer security, pick-up";
            case "38":
                return "PIN tries exceeded, pick-up";
            case "39":
                return "No credit account";
            case "40":
                return "Function not supported";
            case "41":
                return "Lost card";
            case "42":
                return "No universal account";
            case "43":
                return "Stolen card";
            case "44":
                return "No investment account";
            case "51":
                return "Not sufficent funds";
            case "52":
                return "No check account";
            case "53":
                return "No savings account";
            case "54":
                return "Expired card";
            case "55":
                return "Incorrect PIN";
            case "56":
                return "No card record";
            case "57":
                return "Transaction not permitted to cardholder";
            case "58":
                return "Transaction not permitted on terminal";
            case "59":
                return "Suspected fraud";
            case "60":
                return "Contact acquirer";
            case "61":
                return "Exceeds withdrawal limit";
            case "62":
                return "Restricted card";
            case "63":
                return "Security violation";
            case "64":
                return "Original amount incorrect";
            case "65":
                return "Exceeds withdrawal frequency";
            case "66":
                return "Call acquirer security";
            case "67":
                return "Hard capture";
            case "68":
                return "Response received too late";
            case "75":
                return "PIN tries exceeded";
            case "77":
                return "Intervene, bank approval required";
            case "78":
                return "Intervene, bank approval required for partial amount";
            case "90":
                return "Cut-off in progress";
            case "91":
                return "Issuer or switch inoperative";
            case "92":
                return "Routing error";
            case "93":
                return "Violation of law";
            case "94":
                return "Duplicate transaction";
            case "95":
                return "Reconcile error";
            case "96":
                return "System malfunction";
            case "98":
                return " Exceeds cash limit";
            // custom by me
            case "99" : 
                return "no Response"
            case "100" :
                return "Request Timedout"
            case "101":
                return "Failover Direct"
            ///////////////
            default:
                return "unknown";
        }
    },

    getRRN(unpackedMessage){
        return unpackedMessage.dataElements[37];
    },

    /**
     * check if the request data RRN is diffenent from response RRN,
     *  if it's different, there must have been a failover request
     * @param {Object} unpackedMessage unpacked request data from pos
     * @param {Object} unpackedServerResponse response from server
     */
    getFailOverRRN(unpackedMessage,unpackedServerResponse){
        let oldRRN = this.getRRN(unpackedMessage);
        let newRRN = this.getRRN(unpackedServerResponse);
        return oldRRN == newRRN ? '' : newRRN;
    },

    /**
     * get processing code from the unpacked iso message
     * @param {Object} unpackedMessage
     * @returns {String} processCode
     */
    getProcessingCode(unpackedMessage){
        return unpackedMessage.dataElements[3].substr(0, 2);
    },

    /**
     *get the terminal Id from the iso message
     *@param {Object} unpackedMessage
     *@returns {String} terminal Id
     */
    getTerminalId(unpackedMessage){
        return unpackedMessage.dataElements[41];
    },

    /**
     * get key from network message
     * @param  {Object} unpackedMessage
     * @returns {String} key
     */
    getSecurityKey(unpackedMessage){
        if(unpackedMessage.dataElements[53])
            return unpackedMessage.dataElements[53].substring(0,32);
        else return "";
    },

    /**
     * map response code from nibss to it's message
     * @param {String} code response code from nibss
     */
    getNibssResponseMessageFromCode(code) {
        switch (code) {
            case "00":
                return "Approved";
            case "01":
                return "Refer to card issuer";
            case "02":
                return "Refer to card issuer, special condition";
            case "03":
                return "Invalid merchant";
            case "04":
                return "Pick-up card";
            case "05":
                return "Do not honor";
            case "06":
                return "Error";
            case "07":
                return "Pick-up card, special condition";
            case "08":
                return "Honor with identification";
            case "09":
                return "Request in progress";
            case "10":
                return "Approved,partial";
            case "11":
                return "Approved,VIP";
            case "12":
                return "Invalid transaction";
            case "13":
                return "Invalid amount";
            case "14":
                return "Invalid card number";
            case "15":
                return "No such issuer";
            case "16":
                return "Approved,update track 3";
            case "17":
                return "Customer cancellation";
            case "18":
                return "Customer dispute";
            case "19":
                return "Re-enter transaction";
            case "20":
                return "Invalid response";
            case "21":
                return "No action taken";
            case "22":
                return "Suspected malfunction";
            case "23":
                return "Unacceptable transaction fee";
            case "24":
                return "File update not supported";
            case "25":
                return "Unable to locate record";
            case "26":
                return "Duplicate record";
            case "27":
                return "File update edit error";
            case "28":
                return "File update file locked";
            case "29":
                return "File update failed";
            case "30":
                return "Format error";
            case "31":
                return "Bank not supported";
            case "32":
                return "Completed, partially";
            case "33":
                return "Expired card, pick-up";
            case "34":
                return "Suspected fraud, pick-up";
            case "35":
                return "Contact acquirer, pick-up";
            case "36":
                return "Restricted card, pick-up";
            case "37":
                return "Call acquirer security, pick-up";
            case "38":
                return "PIN tries exceeded, pick-up";
            case "39":
                return "No credit account";
            case "40":
                return "Function not supported";
            case "41":
                return "Lost card";
            case "42":
                return "No universal account";
            case "43":
                return "Stolen card";
            case "44":
                return "No investment account";
            case "51":
                return "Not sufficent funds";
            case "52":
                return "No check account";
            case "53":
                return "No savings account";
            case "54":
                return "Expired card";
            case "55":
                return "Incorrect PIN";
            case "56":
                return "No card record";
            case "57":
                return "Transaction not permitted to cardholder";
            case "58":
                return "Transaction not permitted on terminal";
            case "59":
                return "Suspected fraud";
            case "60":
                return "Contact acquirer";
            case "61":
                return "Exceeds withdrawal limit";
            case "62":
                return "Restricted card";
            case "63":
                return "Security violation";
            case "64":
                return "Original amount incorrect";
            case "65":
                return "Exceeds withdrawal frequency";
            case "66":
                return "Call acquirer security";
            case "67":
                return "Hard capture";
            case "68":
                return "Response received too late";
            case "75":
                return "PIN tries exceeded";
            case "77":
                return "Intervene, bank approval required";
            case "78":
                return "Intervene, bank approval required for partial amount";
            case "90":
                return "Cut-off in progress";
            case "91":
                return "Issuer or switch inoperative";
            case "92":
                return "Routing error";
            case "93":
                return "Violation of law";
            case "94":
                return "Duplicate transaction";
            case "95":
                return "Reconcile error";
            case "96":
                return "System malfunction";
            case "98":
                return " Exceeds cash limit";
            // custom by me
            case "99" : 
                return "no Response"
            case "100" :
                return "Request Timedout"
            ///////////////
            default:
                return "unknown";
        }
    },

    getMaskPan(unpackedMessage){
        return unpackedMessage.dataElements[2].substr(0, 6) + ''.padEnd(unpackedMessage.dataElements[2].length - 10, 'X') + unpackedMessage.dataElements[2].slice(-4)
    },

    getCardType(pan){
        let check = pan.substr(0,2);

        if(pan.startsWith("4")) return "VISA";
        else if(["51","52","53","54","55"].includes(check) || pan.startsWith("2")) return "MASTERCARD";
        else if(["50","65"].includes(check)) return "VERVE";
        else if(["34","37"].includes(check)) return "AMERICAN-EXPRESS";
        else if(pan.startsWith("3528") || pan.startsWith("3589")) return "JCB";
        else return "CARD";
    },

    /**
     * read throught the iccdata string, compare tag with the one in config,
     *  read length convert to int , divide by 2 and use the length to read
     *  the tag data. 
     * @param {Object} unpackedMessage unpackmessage object from POS or HOST
     */
    getICCData(unpackedMessage) {
        let nibssICC = unpackedMessage.dataElements[55];

        if (nibssICC) {
            let iccDataList = new Map();
            let skip = 0;
            while (skip < nibssICC.length) {
                let tag = {};
                tag = TlvTags.find(c => c.tag == nibssICC.substr(skip, 2));
                if (tag) {
                    skip = skip + 2;
                } else {
                    tag = TlvTags.find(c => c.tag == nibssICC.substr(skip, 4));
                    skip = skip + 4;
                }

                let length = nibssICC.substr(skip, 2);
                length = (Number.parseInt(length, 16)) * 2;
                skip = skip + 2;
                let data = nibssICC.substr(skip, length);
                // console.log(`tag: ${tag.tag}, data: ${data}`);
                skip = skip + length;
                iccDataList.set(tag.tag, data);
            }

            return iccDataList;
        }
        return false;
    },

    isMitType(unpackedMessage,msgType){
        return unpackedMessage.mti.substring(0, 2) == msgType;
    },

    fileDataLogger(terminalId,msg){
        let enable = process.env.file_log;
        if(enable != "true")return;
        
        let filesPath = path.join(`mw-logs`, terminalId+`-logfile.log`);
        let pathDir = path.dirname(filesPath);
        if (fs.existsSync(pathDir) == false) {
            fs.mkdirSync(pathDir)
        }

        let logger = SimpleNodeLogger.createSimpleLogger({
            logFilePath: filesPath,
            timestampFormat: 'YYYY-MM-DD HH:mm:ss.SSS'
        });

        logger.log('info',msg);
    },

    hmacsha256(data, secret) {

        let sha256 = cryptojs.HmacSHA256(data, secret);
        let base64encoded = cryptojs.enc.Base64.stringify(sha256);
        return base64encoded;
  },

  getBankCodeAndBankName(pan) {
    const checker = pan.substr(0, 6)

   const bankBinObj = bankBins.find(i => i.bin === parseInt(checker));

   const defaultObject = {
        "bank": "DEFAULT",
        "brand": "DEFAULT",
        "disburseBankCode": "000"
    };

    return bankBinObj === undefined ? defaultObject : bankBinObj;

  },

    encryptAES(data,key, keyEncoding,outputEncoding="base64"){
        let d = Buffer.from(data,"utf-8");
        let k = Buffer.from(key,keyEncoding);

        let iv = Buffer.from(crypto.randomBytes(16))
        let cipher = crypto.createCipheriv('aes-128-ecb',k,"");
        cipher.setAutoPadding(true);
        let encryptedData = cipher.update(d,'utf-8',outputEncoding);
        encryptedData+= cipher.final(outputEncoding);
        return encryptedData;
    },

    bankfromTID(terminalId,getCode=false){
        if(!terminalId) return false;
        
        let term = terminalId.substr(0,4);
        if(["2044","2063"].includes(term)){
            if(getCode)return "ACCESS";
            return "ACCESS BANK";
        }
        else if(["2050","2056"].includes(term))
        {
            if(getCode)return "ECO";
            return "ECO BANK";
        }
        else if(["2070"].includes(term))
        {
            if(getCode)return "FIDELITY";
            return "FIDELITY BANK";
        }
        else if(["2011","2071","2701"].includes(term))
        {
            if(getCode)return "FIRST";
            return "FIRST BANK";
        }
        else if(["2214","2085"].includes(term))
        {
            if(getCode) return "FCMB";
            return "FCMB BANK";
        }
        else if(["2030","2084"].includes(term))
        {
            if(getCode)return "HERITAGE";
            return "HERITAGE BANK";
        }
        else if(["2082"].includes(term))
        {
            if(getCode)return "KEYSTONE";
            return "KEYSTONE BANK";
        }
        else if(["2076"].includes(term))
        {
            if(getCode)return "SKYE";
            return "SKYE BANK";
        }
        else if(["2039"].includes(term))
        {
            if(getCode)return "STANBIC";
            return "STANBIC IBTC BANK";
        }
        else if(["2032"].includes(term))
        {
            if(getCode)return "UNION";
            return "UNION BANK";
        }
        else if(["2033"].includes(term))
        {
            if(getCode)return "UBA";
            return "UBA BANK";
        }
        else if(["2035"].includes(term))
        {
            if(getCode)return "WEMA";
            return "WEMA BANK";
        }
        else if(["2057"].includes(term))
        {
            if(getCode)return "ZENITH";
            return "ZENITH BANK";
        }
        else if(["2232"].includes(term))
        {
            if(getCode)return "STERLING";
            return "STERLING BANK";
        }
        else if(["2058"].includes(term))
        {
            if(getCode)return "GTBANK";
            return "GTBANK";
        }
        else if(["2215"].includes(term))
        {
            if(getCode)return "UNITY";
            return "UNITY BANK";
        }
        else if(["2101"].includes(term))
        {
            if(getCode)return "PROVIDUS";
            return "PROVIDUS BANK";
        }
        else{
            return "UNKNOWN";
        }
    },



}