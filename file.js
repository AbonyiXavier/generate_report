async function sendManualPFMNotification(data) {

    const notificationService = {
        "_id":"5d31b9f577e6724950d06cfb",
        "name":"PFM Notification",
        "url":"https://www.pfm.payvice.com/api/tms/iisys/auth2",
        "reversalUrl":"https://www.pfm.payvice.com/api/tms/iisys/auth2",
        "notificationClass":"default",
        "enabled":true
    }

    for(let transaction of data) {

        Journals.findOne({rrn: transaction.rrn, terminalId: transaction.tid, responseCode: "00"}, (err, doc) => {
            // console.log(doc.pfmNotified)
            if(doc.pfmNotified === "FetchError: request to https://www.pfm.payvice.com/api/tms/iisys/auth2 failed, reason: connect ETIMEDOUT 45.79.34.183:443") {

                const pfmNotifier = new PfmNotifier(notificationService, doc);


                pfmNotifier.sendNotification();

            }

        })


    }


}