require("dotenv").config();

const crypto = require('crypto');

const fetch = require('node-fetch');
const Journal = require('./model/journamodel');
const Util = require('./helpers/Utils');
const moment = require('moment');


class IgrParkwayNotifier {
    constructor(notificationService, transactionData, options = {}) {

        this.notificationData = transactionData;
        this.notificationService = notificationService;
        this.options = options;

    }

    signRequest(message, secretKey){
        let encoded = Util.hmacsha256(message, secretKey);
        return encoded;
    }
    async sendNotification() {
        // let theMTIClass = this.notificationData.mti.substr(0, 2);

        // allow notification for declined transactions for test
        if(!(this.notificationData.responseCode === '00' && this.notificationData.MTI == '0200') )
          return false;

        let customerRef = this.notificationData.customerRef;
        if(!customerRef)
        {
            console.error(`Customer Data not found to send notification; IGR-PARKWAY Transaction TID :${this.notificationData.terminalId}, RRN :${this.notificationData.rrn}, at ${new Date().toString()}`);
            Util.fileDataLogger(this.notificationData.terminalId,`Customer Data not found to send notification; IGR-PARKWAY Transaction TID :${this.notificationData.terminalId}, RRN :${this.notificationData.rrn}, at ${new Date().toString()}`);
            return false;
        }

        let CustmerDatas = customerRef.split("~");
        if(CustmerDatas.length < 2)
            return false;

        let invoiceNumber = CustmerDatas[1];


        const bankdetails = Util.getBankCodeAndBankName(this.notificationData.maskedPan);

        
        let theBody = {
            InvoiceNumber: invoiceNumber,
            PaymentRef: this.notificationData.rrn,
            PaymentDate: moment(this.notificationData.transactionTime).format("DD/MM/YYYY HH:MM:SS"),
            //BankCode: bankdetails.disburseBankCode || "000",
            //BankName: bankdetails.bank || "DEFAULT",
            AmountPaid: parseFloat(this.notificationData.amount/100).toFixed(2),
            TransactionDate: moment(this.notificationData.transactionTime).format("DD/MM/YYYY HH:MM:SS"),
            Channel: 'POS',
            PaymentMethod: 'CARD',
            TransactionRefrence: this.notificationData.rrn,
            MaskedPAN: this.notificationData.maskedPan
        };

        console.log('notificationData', theBody);

        //signature digest
        let toHash = theBody.InvoiceNumber + theBody.PaymentRef + theBody.AmountPaid + theBody.PaymentDate + theBody.Channel;
        
        let clientSecret = process.env.igr_parkway_clientsecret;
        let clientId = process.env.igr_parkway_clientid;

        console.log(clientSecret)
        let signatureHash = this.signRequest(toHash, clientSecret);

        console.log(toHash, clientSecret);


        //test
        let notificationHeaders = {
            'Content-Type': 'application/json',
            SIGNATURE : signatureHash,
            CLIENTID: clientId
        }

        console.log(notificationHeaders)


        let notificationBody = JSON.stringify(theBody);

        let requestOptions = {
            method: 'POST',
            headers: notificationHeaders,
            body: notificationBody
        };

        let notificationUrl = this.notificationService.url;

        console.log(`Sending out notification to ${this.notificationService.name}. Notification Body: ${notificationBody}`);
        Util.fileDataLogger(this.notificationData.terminalId,`Sending out  IGR-PARKWAY notification to ${this.notificationService.name}. Notification Body: ${notificationBody}`);

        return new Promise((resolve, reject) => {


        fetch(notificationUrl, requestOptions)
        .then((response) => {
            response.json().then((data) => {

                console.log(`Response from notification of ${this.notificationData._id} from ${this.notificationService.name}. Body: ${JSON.stringify(data)}`);
                // Util.fileDataLogger(this.notificationData.terminalId,`Response from notification of ${this.notificationData._id} from ${this.notificationService.name}. Body: ${JSON.stringify(data)}`);

                Journal.updateOne({_id : this.notificationData._id},{$set : {notified : JSON.stringify(data)}},(err,data)=>{
                    if(err)
                        console.error(`error updating IGR-PARKWAY notification result on journal at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
                    else
                    console.log(` IGR-PARKWAY notification result updated successfully at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
                });

                if(data.Error !== true) {

                    resolve(false)

                }

                resolve(data);


            }).catch((err) => {

                Journal.updateOne({_id : this.notificationData._id},{$set : {notified : response.toString()}},(err,data)=>{
                    if(err)
                        console.error(`error updating  IGR-PARKWAY notification result on journal at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
                    else
                    console.log(` IGR-PARKWAY notification result updated successfully at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
                });

                console.log(`There was an error processing the JSON response from ${this.notificationService.name} for of ${this.notificationData._id}. Error: ${err}. The Response: ${response.toString()}`);
                // Util.fileDataLogger(this.notificationData.terminalId,`There was an error processing the JSON response from ${this.notificationService.name} for of ${this.notificationData._id}. Error: ${err}. The Response: ${response.toString()}`);

                resolve(false);

            });


        })
        .catch((err) => {

            Journal.updateOne({_id : this.notificationData._id},{$set : {notified : err.toString()}},(err,data)=>{
                if(err)
                    console.error(`error updating IGR-PARKWAY notification result on journal at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
                else
                console.log(`IGR-PARKWAY notification result updated successfully at ${new Date().toString()} RRN : ${this.notificationData.rrn}`);
            });

            console.log(`There was an error sending notification of ${this.notificationData._id} to ${this.notificationService.name}. Error: ${err}`);
            // Util.fileDataLogger(this.notificationData.terminalId,`There was an error sending notification of ${this.notificationData._id} to ${this.notificationService.name}. Error: ${err}`);

            resolve(false);
        });





        })

    }
}

module.exports = IgrParkwayNotifier;
